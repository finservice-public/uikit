import { numeral } from './numeral'

export const percentageFormatter = number => numeral(number).format('0.00%')
