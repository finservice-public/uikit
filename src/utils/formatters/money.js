import _ from 'lodash'

export const moneyRubNormalizer = value => {
  const resolve = `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
  const range = resolve.split('.')
  const subText = _.get(range, '1', '')
  const sub = Number(subText.split('').filter(s => s !== '' && s !== ' ').join(''))
  return !sub ? `${range[0]}` : `${range[0]}.${sub < 10 ? subText.length === 1 ? `${sub}0` : `0${sub}` : sub > 100 ? sub.toString().substr(0, 2) : sub}`
}
export const moneyRubDenormalizer = value => value.replace(/(\s*)/g, '')
