import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import Paragraph from 'ui/Typography/Paragraph'
import * as formatters from '../../utils/formatters'

const stories = storiesOf('Formatters', module)

stories.addDecorator(story => (
  <div style={{ width: '40rem' }}>
      {story()}
  </div>
))

const RenderFormatter = ({ value, formatter }) => (
  <Fragment>
    <Paragraph style={{ margin: 0 }}>From: {value}</Paragraph>
    <Paragraph style={{ margin: 0 }}>To: {formatter(value)}</Paragraph>
  </Fragment>
)

stories
  .add('moneyRubNormalizer', () => (
    <RenderFormatter value={10000.234} formatter={formatters.moneyRubNormalizer} />
  ))
  .add('moneyRubNormalizer 08', () => (
    <RenderFormatter value={10000.08} formatter={formatters.moneyRubNormalizer} />
  ))
  .add('moneyRubNormalizer 80', () => (
    <RenderFormatter value={10000.80} formatter={formatters.moneyRubNormalizer} />
  ))
  .add('moneyRubNormalizer 801', () => (
    <RenderFormatter value={10000.801} formatter={formatters.moneyRubNormalizer} />
  ))
  .add('moneyRubNormalizer 666', () => (
    <RenderFormatter value={10000.666} formatter={formatters.moneyRubNormalizer} />
  ))
  .add('moneyRubNormalizer 001', () => (
    <RenderFormatter value={10000.001} formatter={formatters.moneyRubNormalizer} />
  ))
  .add('moneyRubDenormalizer', () => (
    <RenderFormatter value="10000 000.32" formatter={formatters.moneyRubDenormalizer} />
  ))
  .add('phoneNumberNormalizer', () => (
    <RenderFormatter value="9033332211" formatter={formatters.phoneNumberNormalizer} />
  ))
  .add('phoneNumberNormalizer with 1', () => (
    <RenderFormatter value="19033332211" formatter={formatters.phoneNumberNormalizer} />
  ))
  .add('namesNormalizer', () => (
    <RenderFormatter value="Але1123№№;;**##ша" formatter={formatters.namesNormalizer} />
  ))
  .add('percentageFormatter', () => (
    <RenderFormatter value={12.32124214124124} formatter={formatters.percentageFormatter} />
  ))
