export const onlyNumberNormalizer = v => {
  if (v === '-') {
    return v
  }
  const parsed = String(v).replace(/\D/g, '')
  return parsed === '' ? '' : parsed
}
