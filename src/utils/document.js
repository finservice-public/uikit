export function openFileLink(link) {
  const ref = document.createElement('a')
  ref.href = link
  ref.target = '__blank'
  ref.click()
  ref.remove()
}
