export function hexToRgba(h, opacity = 1) {
  const hex = h.replace('#', '')
  let r = 0
  let g = 0
  let b = 0
  if (hex.length === 6) {
    r = parseInt(hex.substring(0, 2), 16)
    g = parseInt(hex.substring(2, 4), 16)
    b = parseInt(hex.substring(4, 6), 16)
  } else {
    const rd = hex.substring(0, 1) + hex.substring(0, 1)
    const gd = hex.substring(1, 2) + hex.substring(1, 2)
    const bd = hex.substring(2, 3) + hex.substring(2, 3)
    r = parseInt(rd, 16)
    g = parseInt(gd, 16)
    b = parseInt(bd, 16)
  }

  return `rgba(${r}, ${g}, ${b}, ${opacity})`
}

export function subtractLight(color, amount) {
  const cc = parseInt(color, 16) - amount
  const c = (cc < 0) ? 0 : (cc)
  return c.toString(16).length > 1 ? c.toString(16) : `0${c.toString(16)}`
}

export function darken(color, amount) {
  color = (color.indexOf('#') >= 0) ? color.substring(1, color.length) : color
  amount = parseInt(255 * amount)
  return `#${subtractLight(color.substring(0, 2), amount)}${subtractLight(color.substring(2, 4), amount)}${subtractLight(color.substring(4, 6), amount)}`
}

export function addLight(color, amount) {
  const cc = parseInt(color, 16) + amount
  const c = (cc > 255) ? 255 : (cc)
  return c.toString(16).length > 1 ? c.toString(16) : `0${c.toString(16)}`
}


export function lighten(color, amount) {
  color = (color.indexOf('#') >= 0) ? color.substring(1, color.length) : color
  amount = parseInt(255 * amount)
  return `#${addLight(color.substring(0, 2), amount)}${addLight(color.substring(2, 4), amount)}${addLight(color.substring(4, 6), amount)}`
}
