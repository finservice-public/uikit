export const elementIdGenerator = () => {
  const length = 16
  const hexDigits = '0123456789abcdef'
  const s = parseInt(String(Math.random()).slice(2), 10)
    .toString('16')
    .split('')
    .slice(0, length)

  // eslint-disable-next-line
  s[length - 3] = hexDigits.substr((s[length - 3] & 0x3) | 0x8, 1)
  return s.join('')
}
