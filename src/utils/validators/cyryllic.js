export const Cyryllic = /^[А-яё]+$/i
export const CyrillicName = /^[А-яё-]+$/i
export const CyrillicNameWithSpace = /^[\sА-яё\-]+$/i

export const cyrillicPartFullName = /[ЁА-яё]+((\s|-)[ЁА-яё]+)?/
export const CyrillicFullName = new RegExp(`${cyrillicPartFullName.source}\\s${cyrillicPartFullName.source}(\\s${cyrillicPartFullName.source})?$`)
