import { isSafe } from 'utils/object'

export const validRequired = (field) => {
  if (!isSafe(field)) return 'Обязательное поле'
  if (`${field}`.trim() === '') return 'Обязательное поле'
  return null
}
