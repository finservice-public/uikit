import _ from 'lodash'

export const applyHtmlData = (fields = {}) => {
  if (!fields || _.isEmpty(fields)) return {}
  const resolve = {}
  _.each(fields, (v, k) => {
    resolve[`data-${k}`] = v
  })
  return resolve
}
