import moment from './moment'

const DATE_FORMATS = [
  'L', 'DD.MM.YYYY HH:mm:ss',
  'DD.MM.YYYY', 'YYYY-MM-DD',
  'YYYY-MM-DDTHH:mm:ssZ'
]

export const DATE_FORMAT = 'DD.MM.YYYY'

export const FromNow = (date) => {
  if (!date) { return 'никогда' }
  return moment(date).fromNow()
}

export const FormatDate = (date, format = 'dddd, DD MMMM YYYY, HH:mm') => {
  if (date) {
    return moment(date, DATE_FORMATS).format(format)
  }
  return ''
}

export const parseDate = date => moment(date, DATE_FORMATS)

export const dateLessThan = (date, lessThan, message) => {
  const valueMoment = parseDate(date)
  const dateMoment = parseDate(lessThan)

  if (valueMoment.isValid() && dateMoment.isValid() && !valueMoment.isBefore(dateMoment)) {
    return message
  }
  return null
}


export const dateGreaterThan = (date, greaterThan, message) => {
  const valueMoment = parseDate(date)
  const dateMoment = parseDate(greaterThan)

  if (valueMoment.isValid() && dateMoment.isValid() && !valueMoment.isAfter(dateMoment)) {
    return message
  }
  return null
}

export const lessThanCurrentDate = date => {
  if (!parseDate(date).isBefore(moment())) {
    return 'Не может быть больше текущей даты'
  }
  return null
}

export const isEqualDates = (firstDate, secondDate) => {
  if (!firstDate || !secondDate) return false

  return parseDate(firstDate).isSame(parseDate(secondDate), 'day') &&
    parseDate(firstDate).isSame(parseDate(secondDate), 'month') &&
    parseDate(firstDate).isSame(parseDate(secondDate), 'year')
}

export default DATE_FORMATS
