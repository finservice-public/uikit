import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import Paragraph from './Paragraph'
import Divider from '../Divider'

const stories = storiesOf('Typography/Paragraph', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

const text = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquam autem cum delectus dolore dolorem dolores in incidunt itaque iure modi numquam perferendis, quae qui tenetur vitae voluptas! Nihil, voluptates?'

stories
  .add('default', () => (
    <Fragment>
      <Paragraph>{text}</Paragraph>
      <Paragraph>{text}</Paragraph>
    </Fragment>
  ))
  .add('secondary', () => (
    <Fragment>
      <Paragraph type="secondary">{text}</Paragraph>
      <Paragraph type="secondary">{text}</Paragraph>
    </Fragment>
  ))
  .add('primary', () => (
    <Fragment>
        <Paragraph type="primary">{text}</Paragraph>
        <Paragraph type="primary">{text}</Paragraph>
    </Fragment>
  ))
  .add('success', () => (
    <Fragment>
      <Paragraph type="success">{text}</Paragraph>
      <Paragraph type="success">{text}</Paragraph>
    </Fragment>
  ))
  .add('danger', () => (
    <Fragment>
      <Paragraph type="danger">{text}</Paragraph>
      <Paragraph type="danger">{text}</Paragraph>
    </Fragment>
  ))
  .add('danger', () => (
    <Fragment>
      <Paragraph type="danger">{text}</Paragraph>
      <Paragraph type="danger">{text}</Paragraph>
    </Fragment>
  ))
  .add('warning', () => (
    <Fragment>
      <Paragraph type="warning">{text}</Paragraph>
      <Paragraph type="warning">{text}</Paragraph>
    </Fragment>
  ))
  .add('mark', () => (
    <Fragment>
      <Paragraph isMark>{text}</Paragraph>
      <Paragraph isMark>{text}</Paragraph>
    </Fragment>
  ))
  .add('deleted', () => (
    <Fragment>
      <Paragraph isDelete>{text}</Paragraph>
      <Paragraph isDelete>{text}</Paragraph>
    </Fragment>
  ))
  .add('underline', () => (
    <Fragment>
      <Paragraph isUnderline>{text}</Paragraph>
      <Paragraph isUnderline>{text}</Paragraph>
    </Fragment>
  ))
  .add('strong and medium', () => (
    <Fragment>
      <Paragraph isStrong>{text}</Paragraph>
      <Divider/>
      <Paragraph isMedium>{text}</Paragraph>
    </Fragment>
  ))
  .add('ellipsis', () => (
    <Fragment>
      <Paragraph isEllipsis>
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText.
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText.
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText.
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText.
      </Paragraph>
      <Paragraph isEllipsis>
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText.
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText.
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText.
        LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText LongTextLongTextLongTextLongTextLongText.
      </Paragraph>
    </Fragment>
  ))
  .add('align', () => (
    <Fragment>
        <Paragraph>{text}</Paragraph>
        <Paragraph align="center">{text}</Paragraph>
        <Paragraph align="right">{text}</Paragraph>
    </Fragment>
  ))
