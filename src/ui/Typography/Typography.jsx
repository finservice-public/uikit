import React from 'react'
import Text from './Text'
import Paragraph from './Paragraph'
import Title from './Title'
import Divider from './Divider'

export { Text, Paragraph, Title, Divider }
