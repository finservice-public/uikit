import React from 'react'
import { storiesOf } from '@storybook/react'
import Collapse, { Panel } from './Collapse'

const stories = storiesOf('Collapse', module)

stories.addDecorator(story => (
  <div style={{ width: '40rem' }}>
    {story()}
  </div>
))

const panels = [
  <Panel key="1" header="Panel 1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, voluptatum.</Panel>,
  <Panel key="2" header="Panel 2">Lorem ipsum dolor sit amet.</Panel>,
  <Panel key="3" header="Panel 3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam, nisi?</Panel>,
  <Panel key="4" header="Panel 4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto at esse porro? Cupiditate facilis in iusto provident repellat? Assumenda blanditiis deserunt dolore, minus similique veniam.</Panel>
]

stories
  .add('default', () => (
    <Collapse defaultKeys={['2']}>
      {panels}
    </Collapse>
  ))
  .add('accordion', () => (
    <Collapse defaultKeys={['2']} isAccordion>
      {panels}
    </Collapse>
  ))
  .add('bordered', () => (
    <Collapse defaultKeys={['2']} isBorder>
      {panels}
    </Collapse>
  ))
  .add('right arrow', () => (
    <Collapse defaultKeys={['2']} arrowPlacement="right">
      {panels}
    </Collapse>
  ))
  .add('without divider', () => (
    <Collapse defaultKeys={['1']} arrowPlacement="right" isDivider={false}>
      {panels}
    </Collapse>
  ))
  .add('single panel & without divider without padding', () => (
    <Collapse defaultKeys={['1']} arrowPlacement="right" isPadding={false} isDivider={false}>
      <Panel key="1" header="Panel 1">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, voluptatum.</Panel>
    </Collapse>
  ))
