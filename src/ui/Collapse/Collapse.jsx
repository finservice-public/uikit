import _ from 'lodash'
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import AntdCollapse from 'antd/es/collapse'
import Icon from 'ui/Icon'
import styled from 'styled-components'
import withMargin from 'ui/common/withMargin'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject, isSafe } from 'utils/object'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isDivider')
  _.unset(resolveProps, 'isPadding')
  return <AntdCollapse {...resolveProps}/>
})`
  background-color: transparent !important;
  
  & .ant-collapse-header {
    padding: ${props => (props.isPadding ? '12px 16px' : '0 !important')};
    padding-right: ${props => (props.isPadding ? '40px' : '0 !important')};
    user-select: none;
  }
  
  & .ant-collapse-content-box {
    padding: ${props => (props.isPadding ? '16px' : '0 !important')};
    padding-top: ${props => (props.isPadding ? '4px' : '0 !important')};
  }

  & > div {
    border-bottom: ${props => (props.isDivider ? '1px solid #d9d9d9' : 'none !important')};
  }
`

const { Panel } = AntdCollapse

const Collapse = ({
  className, style,
  htmlID, dataSet,
  isBorder,
  isAccordion,
  arrowPlacement,
  defaultKeys,
  isDivider,
  isPadding,
  expandIconProps,
  children,
  onChange,
  ...props
}) => {
  const [activeKey, setActiveKey] = useState(null)

  const handleChange = v => {
    setActiveKey(v)
    if (onChange) onChange(v)
  }

  useEffect(() => {
    if (isSafe(props.activeKey)) {
      setActiveKey(props.activeKey)
    }
  }, [props.activeKey])

  return (
    <Root
      className={className}
      style={style}
      bordered={isBorder}
      isPadding={isPadding}
      expandIcon={({ isActive }) => (
        <Icon
          iconType="design"
          icon="expand-up"
          rotate={isActive ? 0 : 180}
          {...expandIconProps}
        />
      )}
      expandIconPosition={arrowPlacement}
      accordion={isAccordion}
      isDivider={isDivider}
      {...safeObject({
        activeKey: onChange ? activeKey : null,
        onChange: onChange ? handleChange : null,
        defaultActiveKey: !_.isEmpty(defaultKeys) ? defaultKeys : null,
        htmlID: htmlID || null
      })}
      {...applyHtmlData(dataSet)}
    >
      {children}
    </Root>
  )
}

Collapse.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,

  htmlID: PropTypes.any,
  dataSet: PropTypes.object,
  children: PropTypes.any,
  isBorder: PropTypes.bool,
  isAccordion: PropTypes.bool,
  arrowPlacement: PropTypes.oneOf(['left', 'right']),
  defaultKeys: PropTypes.array,
  activeKey: PropTypes.any,
  isDivider: PropTypes.bool,
  isPadding: PropTypes.bool,
  expandIconProps: PropTypes.object,

  onChange: PropTypes.func
}

Collapse.defaultProps = {
  children: [],
  isBorder: false,
  isAccordion: false,
  arrowPlacement: 'left',
  defaultKeys: null,
  activeKey: null,
  isDivider: true,
  isPadding: true,
  expandIconProps: null
}

Collapse.displayName = 'Collapse'
Panel.displayName = 'CollapsePanel'

export { Panel }

export default withMargin({ displayName: 'Collapse' })(Collapse)
