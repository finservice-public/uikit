import _ from 'lodash'
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import AntTable from 'antd/es/table'
import withMargin from 'ui/common/withMargin'
import Icon from 'ui/Icon'
import { unsetFields } from 'utils/props'
import Button from 'ui/Button'
import { Row, Col } from 'ui/Grid'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'
import { colors } from 'styles'
import styled from 'styled-components'
import Block from 'ui/Block'
import LazyPagination from './LazyPagination'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isFirstUnderline')
  return <AntTable {...resolveProps}/>
})`
  & tbody > tr:first-child > td {
    border-top: ${props => (props.isFirstUnderline ? `1px solid ${colors.gray2}` : 'none')};
  }
`

const Table = ({
  className, style, htmlID, dataSet,

  title, columns, rowKey, data, total, isLoading, paginationPlacement, isPagination,
  isMore, moreLabel, moreLabelExpand, moreCount, moreLabelPlacement,
  size, isShowHeader, footer, isBordered, isLazy,
  expandedRowRender, rowClassName, isScrollX, expandRowByClick,

  onChange, onRowClick,
  ...props
}) => {
  const [page, setPage] = useState(1)
  const [pageSize, setPageSize] = useState(10)
  const [isExpand, setIsExpand] = useState(false)

  useEffect(() => {
    setPage(props.page)
    setPageSize(props.pageSize)
  }, [props.page, props.pageSize])

  const handleChangePagination = (pagination, filters, sorter) => {
    setPage(pagination.current)
    setPageSize(pagination.pageSize)
    if (onChange) {
      onChange({
        pagination: {
          page: _.get(pagination, 'current', null) || page,
          pageSize: _.get(pagination, 'pageSize', null) || pageSize
        },
        sorter: _.get(sorter, 'field', null) ? {
          field: sorter.field,
          order: sorter.order
        } : null
      })
    }
  }

  const handleClickRow = (record, index) => () => {
    if (onRowClick) onRowClick(record, index)
  }

  const handleExpand = () => setIsExpand(!isExpand)

  return (
    <>
      <Root
        isFirstUnderline={!isShowHeader}
        isExpand={isExpand}
        className={className}
        style={safeObject({
          ...style,
          cursor: isLoading ? 'not-allowed' : null
        })}
        dataSource={!isMore ? data :
          !isExpand ? [...data].splice(0, moreCount) : data}
        loading={isLoading}
        pagination={isPagination && !isMore && !isLazy ? safeObject({
          total: total !== -1 ? total : null,
          current: page,
          pageSize,
          position: paginationPlacement
        }) : false}
        showHeader={isShowHeader}
        onChange={handleChangePagination}
        bordered={isBordered}
        expandedRowRender={expandedRowRender}
        expandRowByClick={expandRowByClick}
        rowClassName={rowClassName}
        {...safeObject({
          size: size === 'lg' ? null : size === 'sm' ? 'middle' : 'small',
          footer: footer ? () => footer : isMore && data.length > moreCount ?
            () => (
                <Row
                  isFlex
                  justify={(() => {
                    switch (moreLabelPlacement) {
                      case 'left':
                        return 'start'
                      case 'right':
                        return 'end'
                      case 'center':
                        return 'center'
                      default:
                        return 'center'
                    }
                  })()}
              >
                <Col>
                  <Button
                      icon={!isExpand ? 'down' : 'up'}
                      size="sm"
                      onClick={handleExpand}
                  >
                    {!isExpand ? moreLabel : moreLabelExpand}
                  </Button>
                </Col>
              </Row>
            ) : null,
          title: title ? () => title : null,
          scroll: isScrollX ? { x: true } : null,
          onRow: onRowClick ? (record, index) => ({ onClick: handleClickRow(record, index) }) : null,
          rowKey,
          expandIcon: expandedRowRender ? expandProps => (
              <Icon
                  icon={expandProps.expanded ? 'caret-up' : 'caret-down'}
                  onClick={e => expandProps.onExpand(expandProps.record, e)}
              />
          ) : null,
          id: htmlID || null
        })}
        {...applyHtmlData(dataSet)}
      >
        {_.map(columns, (col, index) => (
            <AntTable.Column
                key={`${col.key}-${index}`}
                {...unsetFields(['key'], col)}
                {...safeObject({
                  id: htmlID ? `${htmlID}-${index}` : null
                })}
            />
        ))}
      </Root>
      {isLazy &&
      <Block isCenter isMarginTop marginTop={2}>
        <LazyPagination
          htmlID={htmlID}
          page={page}
          isDisable={isLoading}
          onChange={current => {
            handleChangePagination({ current, pageSize })
          }}
          data={data}
          pageSize={pageSize}
        />
      </Block>
      }
    </>
  )
}

Table.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  page: PropTypes.number,
  pageSize: PropTypes.number,
  total: PropTypes.number,
  columns: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.any,
    dataIndex: PropTypes.string,
    key: PropTypes.string,
    render: PropTypes.func,
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    className: PropTypes.string,
    align: PropTypes.oneOf(['left', 'center', 'right']),
    sortOrder: PropTypes.oneOf(['ascend', 'descend', false])
  })),
  rowKey: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  expandedRowRender: PropTypes.func,
  expandRowByClick: PropTypes.bool,
  rowClassName: PropTypes.func,
  data: PropTypes.arrayOf(PropTypes.object),
  isLoading: PropTypes.bool,
  isPagination: PropTypes.bool,
  paginationPlacement: PropTypes.oneOf(['bottom', 'top', 'both']),
  size: PropTypes.oneOf(['xs', 'sm', 'lg']),
  isMore: PropTypes.bool,
  moreLabel: PropTypes.string,
  moreLabelExpand: PropTypes.string,
  moreCount: PropTypes.number,
  moreLabelPlacement: PropTypes.oneOf(['left', 'center', 'right']),
  isShowHeader: PropTypes.bool,
  title: PropTypes.any,
  footer: PropTypes.any,
  isBordered: PropTypes.bool,
  isScrollX: PropTypes.bool,

  isLazy: PropTypes.bool,

  onChange: PropTypes.func,
  onRowClick: PropTypes.func
}

Table.defaultProps = {
  columns: [],
  rowKey: null,
  data: [],
  isLoading: false,
  total: -1,
  page: 1,
  pageSize: 10,
  paginationPlacement: 'both',
  isPagination: true,
  isMore: false,
  moreLabel: 'Раскрыть',
  moreLabelExpand: 'Скрыть',
  moreCount: 5,
  moreLabelPlacement: 'right',
  size: 'lg',
  isShowHeader: true,
  title: null,
  footer: null,
  isBordered: false,
  isScrollX: false,
  isLazy: false
}

Table.displayName = 'Table'

export default withMargin({ displayName: 'Table' })(Table)
