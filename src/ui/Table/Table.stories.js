import _ from 'lodash'
import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Tag from 'ui/Tag'
import Button, { ButtonGroup } from 'ui/Button'
import { Paragraph, Title, Text, Divider } from 'ui/Typography'
import { createGlobalStyle } from 'styled-components'
import Table from './Table'

const RowSelect = createGlobalStyle`
  .custom-row-select:hover {
    box-shadow: 0 2px 8px rgba(13, 21, 43, 0.09)
  }
`

const stories = storiesOf('Table', module)

stories.addDecorator(story => (
    <div style={{ width: '80%' }}>
        {story()}
    </div>
))


const data = _.map(_.times(100), t => ({
  number: t + 1,
  name: `Name ${t + 1}`,
  age: (t + 1) + 20,
  address: `Address ${t + 1}`
}))

const lazyData = _.map(_.times(5), t => ({
  number: t + 1,
  name: `Name ${t + 1}`,
  age: (t + 1) + 20,
  address: `Address ${t + 1}`
}))

const columns = [
  { title: 'Number', dataIndex: 'number' },
  { title: 'Name', dataIndex: 'name' },
  { title: 'Age', dataIndex: 'age' },
  { title: 'Address', dataIndex: 'address' }
]

stories
  .add('default', () => (
    <Table
        columns={columns}
        data={data}
        onChange={action('changed')}
        onRowClick={action('row click')}
    />
  ))
  .add('pagination bottom', () => (
    <Table
        columns={columns}
        data={data}
        paginationPlacement="bottom"
        onChange={action('changed')}
        onRowClick={action('row click')}
    />
  ))
  .add('pagination top', () => (
    <Table
        columns={columns}
        data={data}
        paginationPlacement="top"
        onChange={action('changed')}
        onRowClick={action('row click')}
    />
  ))
  .add('loading', () => (
    <Table
        columns={columns}
        data={data}
        isLoading
        onChange={action('changed')}
        onRowClick={action('row click')}
    />
  ))
  .add('bordered', () => (
    <Table
        columns={columns}
        data={data}
        onChange={action('changed')}
        onRowClick={action('row click')}
        isBordered
    />
  ))
  .add('sizes', () => (
    <Fragment>
        <Table
            columns={columns}
            data={data}
            size="xs"
            onChange={action('changed')}
            onRowClick={action('row click')}
        />
        <Table
            columns={columns}
            data={data}
            size="sm"
            onChange={action('changed')}
            onRowClick={action('row click')}
        />
        <Table
            columns={columns}
            data={data}
            size="lg"
            onChange={action('changed')}
            onRowClick={action('row click')}
        />
    </Fragment>
  ))
  .add('without header', () => (
    <Table
        columns={columns}
        data={data}
        isShowHeader={false}
        onChange={action('changed')}
        onRowClick={action('row click')}
    />
  ))
  .add('with footer', () => (
    <Table
        columns={columns}
        data={data}
        onChange={action('changed')}
        onRowClick={action('row click')}
        footer={
            <Paragraph isStrong>
                Footer
            </Paragraph>
        }
    />
  ))
  .add('with title', () => (
    <Table
        columns={columns}
        data={data}
        onChange={action('changed')}
        onRowClick={action('row click')}
        title={
            <Title>
                Title
            </Title>
        }
    />
  ))
  .add('with column width', () => (
    <Table
        columns={[
          { title: 'Number', dataIndex: 'number', width: '5rem' },
          { title: 'Name', dataIndex: 'name', width: '10rem' },
          { title: 'Age', dataIndex: 'age', width: '10rem' },
          { title: 'Address', dataIndex: 'address' }
        ]}
        data={data}
        onChange={action('changed')}
        onRowClick={action('row click')}
    />
  ))
  .add('custom column render', () => (
    <Table
        columns={[
          { title: 'Number', dataIndex: 'number', width: '5rem' },
          { title: 'Name', dataIndex: 'name', width: '10rem' },
          {
            title: 'Age',
            dataIndex: 'age',
            render: (text, record, index) => (
                <Fragment>
                    <Tag>old</Tag>
                    <Tag color="success">year</Tag>
                    <Tag color="primary">{record.age}</Tag>
                </Fragment>
            ),
            width: '20rem'
          },
          { title: 'Address', dataIndex: 'address' },
          {
            render: (text, record, index) => (
                <ButtonGroup>
                    <Button size="sm">Create</Button>
                    <Button size="sm">Update</Button>
                    <Button size="sm" type="danger" isFill={false}>Delete</Button>
                </ButtonGroup>
            )
          }
        ]}
        data={data}
        onChange={action('changed')}
        onRowClick={action('row click')}
    />
  ))
  .add('sorter columns', () => (
    <Table
        columns={[
          { title: 'Number', dataIndex: 'number', width: '5rem', sorter: true },
          { title: 'Name', dataIndex: 'name', width: '10rem' },
          { title: 'Age', dataIndex: 'age', width: '10rem', sorter: true },
          { title: 'Address', dataIndex: 'address' }
        ]}
        data={data}
        onChange={action('changed')}
        onRowClick={action('row click')}
    />
  ))
  .add('expand row', () => (
      <Table
          columns={[
            { title: 'Number', dataIndex: 'number', width: '5rem' },
            { title: 'Name', dataIndex: 'name', width: '10rem' },
            {
              title: 'Age',
              dataIndex: 'age',
              render: (text, record, index) => (
                      <Fragment>
                          <Tag>old</Tag>
                          <Tag color="success">year</Tag>
                          <Tag color="primary">{record.age}</Tag>
                      </Fragment>
              ),
              width: '20rem'
            },
            { title: 'Address', dataIndex: 'address' },
            {
              render: (text, record, index) => (
                      <ButtonGroup>
                          <Button size="sm">Create</Button>
                          <Button size="sm">Update</Button>
                          <Button size="sm" type="danger" isFill={false}>Delete</Button>
                      </ButtonGroup>
              )
            }
          ]}
          data={data}
          onChange={action('changed')}
          onRowClick={action('row click')}
          expandRowByClick
          expandedRowRender={(record, index) => (
              <Fragment>
                <Text isStrong>Name: </Text>{record.name}
                <Divider/>
                <Text isStrong>Age: </Text>{record.name}
                <Divider/>
                <Text isStrong>Address: </Text>{record.name}
              </Fragment>
          )}
      />
  ))
  .add('more', () => (
        <Table
            columns={[
              { title: 'Number', dataIndex: 'number', width: '5rem' },
              { title: 'Name', dataIndex: 'name', width: '10rem' },
              {
                title: 'Age',
                dataIndex: 'age',
                render: (text, record, index) => (
                        <Fragment>
                            <Tag>old</Tag>
                            <Tag color="success">year</Tag>
                            <Tag color="primary">{record.age}</Tag>
                        </Fragment>
                ),
                width: '20rem'
              },
              { title: 'Address', dataIndex: 'address' },
              {
                render: (text, record, index) => (
                        <ButtonGroup>
                            <Button size="sm">Create</Button>
                            <Button size="sm">Update</Button>
                            <Button size="sm" type="danger" isFill={false}>Delete</Button>
                        </ButtonGroup>
                )
              }
            ]}
            data={data}
            onChange={action('changed')}
            onRowClick={action('row click')}
            expandedRowRender={(record, index) => (
                <Fragment>
                    <Text isStrong>Name: </Text>{record.name}
                    <Divider/>
                    <Text isStrong>Age: </Text>{record.name}
                    <Divider/>
                    <Text isStrong>Address: </Text>{record.name}
                </Fragment>
            )}
            isPagination={false}
            isMore
            isBordered
        />
  ))
  .add('styled row', () => (
    <>
        <RowSelect/>
        <Table
          columns={columns}
          data={data}
          onChange={action('changed')}
          onRowClick={action('row click')}
          rowClassName={() => 'custom-row-select'}
        />
    </>
  ))
  .add('lazy', () => (
    <Table
        columns={columns}
        data={lazyData}
        onChange={action('changed')}
        onRowClick={action('row click')}
        isLazy
        isShowHeader={false}
        pageSize={5}
    />
  ))
