import _ from 'lodash'
import fp from 'lodash/fp'
import React from 'react'
import PropTypes from 'prop-types'
import AntMenu from 'antd/es/menu'
import withMargin from 'ui/common/withMargin'
import styled from 'styled-components'
import { colors } from 'styles'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Root = styled(AntMenu)`
  user-select: none;
  
  & * {
    user-select: none;
  }
  
  & .ant-menu-item-selected {
     color: ${colors.primary};
     font-weight: 500;
  }
`

const Menu = ({
  style, className, htmlID, dataSet,
  options, id, onSelect
}) => {
  const handleSelect = ({ key }) => {
    const option = _.find(options, o => o.id === key)
    if (!option) onSelect(key)
    if (_.isEmpty(option.options)) onSelect(key)
  }

  const openKeys = _.chain(options)
    .filter(option => !_.isEmpty(option.options) &&
        _.findIndex(option.options, o => o.id === id) !== -1)
    .map(option => option.id)
    .value()

  return (
        <Root
            style={style}
            className={className}
            selectedKeys={[id]}
            mode="inline"
            {...safeObject({
              onSelect: onSelect ? handleSelect : null,
              selectedKeys: id !== '' && id !== undefined ? [id] : null,
              defaultOpenKeys: !_.isEmpty(openKeys) ? openKeys : null
            })}
            {...safeObject({
              id: htmlID || null
            })}
            {...applyHtmlData(dataSet)}
        >
            {fp.map(option => (
              _.isEmpty(option.options) ?
                    <AntMenu.Item key={option.id} {...safeObject({ id: htmlID ? `${htmlID}-${option.id}` : null })}>
                        {option.node}
                    </AntMenu.Item> :
                    <AntMenu.SubMenu
                        key={option.id}
                        title={option.node}
                        {...safeObject({
                          onTitleClick: onSelect ? handleSelect : null,
                          id: htmlID ? `${htmlID}-${option.id}` : null
                        })}
                    >
                        {fp.map(subOption => (
                            <AntMenu.Item key={subOption.id} {...safeObject({ id: htmlID ? `${htmlID}-${option.id}-${subOption.id}` : null })}>
                                {subOption.node}
                            </AntMenu.Item>
                        ))(option.options)}
                    </AntMenu.SubMenu>
            ))(options)}
        </Root>
  )
}

Menu.propTypes = {
  style: PropTypes.object,
  className: PropTypes.string,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  options: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    node: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
    options: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string.isRequired,
      node: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired
    }))
  })),
  id: PropTypes.string,
  onSelect: PropTypes.func
}

Menu.defaultProps = {
  options: [],
  id: ''
}

Menu.displayName = 'Menu'

export default withMargin({ displayName: 'Menu', forceIsRem: false })(Menu)
