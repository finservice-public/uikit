import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Menu from './Menu'

const options = [
  { id: 'one', node: 'One' },
  { id: 'two', node: 'Two' },
  { id: 'three', node: 'Three' },
  { id: 'four', node: 'Four' },
  { id: 'five', node: 'Five' }
]

const subOptions = [
  { id: 'one', node: 'One' },
  {
    id: 'two',
    node: 'Two',
    options: [
      { id: 'two-1', node: 'Two 1' },
      { id: 'two-2', node: 'Two 2' },
      { id: 'two-3', node: 'Two 3' }
    ]
  },
  { id: 'three', node: 'Three' },
  {
    id: 'four',
    node: 'Four',
    options: [
      { id: 'four-1', node: 'Four 1' },
      { id: 'four-2', node: 'Four 2' },
      { id: 'four-3', node: 'Four 3' }
    ]
  },
  { id: 'five', node: 'Five' }
]

const stories = storiesOf('Menu', module)

stories
  .add('default', () => (
    <Menu
        options={options}
        width={20}
        onSelect={action('select')}
    />
  ))
  .add('selected', () => (
    <Menu
        options={options}
        width={20}
        id="one"
        onSelect={action('select')}
    />
  ))
  .add('sub options with select', () => (
    <Menu
        options={subOptions}
        width={20}
        onSelect={action('select')}
        id="three"
    />
  ))
  .add('sub options with sub select', () => (
    <Menu
        options={subOptions}
        width={20}
        onSelect={action('select')}
        id="two-1"
    />
  ))
