import React, { Fragment, useState } from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import storiesHelpers from 'ui/common/withFormProps/storiesHelpers'
import Tag from 'ui/Tag'
import Icon from 'ui/Icon'
import InputSelect from './InputSelect'

const stories = storiesOf('InputSelect', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

const options = [
  { value: 'one', label: 'One' },
  { value: 'two', label: 'Two' },
  { value: 'three', label: 'Three' },
  { value: 'four', label: 'Four' },
  { value: 'five', label: 'Five' },
  { value: 'six', label: 'Six' },
  { value: 'seven', label: 'Seven' }
]

stories
  .add('default', () => {
    const [value, setValue] = useState(null)

    return (
        <InputSelect options={options} onChange={setValue} value={value}/>
    )
  })
  .add('with clear', () => (
    <InputSelect options={options} onChange={action('changed')} isAllowClear/>
  ))
  .add('saving input & simple autocomplete', () => {
    const [value, setValue] = useState('Two')
    return (
          <InputSelect
              options={['One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven']}
              isSelect={false}
              value={value}
              onChange={setValue}
          />
    )
  })
  .add('with placeholder', () => (
    <InputSelect placeholder="Select any" options={options} onChange={action('changed')}/>
  ))
  .add('with left & right', () => (
    <InputSelect
        label="Count"
        left="Sum"
        right={<Icon icon="info-circle"/>}
        onClickLeft={action('left click')}
        onClickRight={action('right click')}
        options={options}
        onChange={action('changed')}
    />
  ))
  .add('sizes', () => (
      <Fragment>
          <InputSelect options={options} onChange={action('changed')} size="xs" isMarginBottom/>
          <InputSelect options={options} onChange={action('changed')} size="sm" isMarginBottom/>
          <InputSelect options={options} onChange={action('changed')} size="lg"/>
      </Fragment>
  ))
  .add('disabled', () => (
    <>
        <InputSelect options={options} onChange={action('changed')} isDisable isMarginBottom/>
        <InputSelect options={options} onChange={action('changed')} isDisable label="Label" isMarginBottom/>
        <InputSelect options={options} onChange={action('changed')} isDisable size="sm" isMarginBottom/>
        <InputSelect options={options} onChange={action('changed')} isDisable size="sm" label="Label" isMarginBottom/>
        <InputSelect options={options} onChange={action('changed')} isDisable size="xs" isMarginBottom/>
        <InputSelect options={options} onChange={action('changed')} isDisable size="xs" label="Label"/>
    </>
  ))
  .add('force opened', () => (
    <InputSelect options={options} onChange={action('changed')} isForceOpen/>
  ))
  .add('custom dropdown style', () => (
    <InputSelect
        onChange={action('changed')}
        options={[
          { value: 'one', label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, sunt?' },
          { value: 'two', label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum explicabo nam non odio similique. Adipisci laboriosam magnam nam repudiandae soluta!' },
          { value: 'three', label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus consequuntur eius, laudantium minima mollitia quasi qui quo quod rerum velit. Deserunt distinctio et eum facere voluptate? Alias culpa dignissimos ducimus inventore itaque, nulla temporibus! Architecto autem debitis, et explicabo ipsum magnam obcaecati officiis, perferendis recusandae saepe sint soluta ut voluptates!' }
        ]}
        dropdownStyle={{
          width: '50rem'
        }}
    />
  ))
  .add('with node option', () => {
    const [value, setValue] = useState(null)
    return (
        <InputSelect
            width={15}
            value={value}
            onChange={setValue}
            options={[
              { value: 'one', label: 'One', node: <Tag color="primary">One</Tag> },
              { value: 'two', label: 'Two', node: <Tag color="success">Two</Tag> },
              { value: 'three', label: 'Three', node: <Tag color="warning">Three</Tag> },
              { value: 'four', label: 'Four', node: <Tag>Four</Tag> }
            ]}
        />
    )
  })
  .add('with blur', () => (
        <InputSelect
            options={options}
            onChange={action('changed')}
            onChangeText={action('changed')}
            onBlur={action('blur')}
            onFocus={action('focus')}
            isSelect={false}
        />
  ))
  .add('with html id', () => (
    <InputSelect options={options} onChange={action('changed')} htmlID="example-select"/>
  ))
  .add('with postfix', () => (
    <InputSelect
        options={options}
        onChange={action('changed')}
        htmlID="example-select"
        postfix={<Icon icon="eye"/>}
    />
  ))

storiesHelpers({
  stories,
  Component: props => <InputSelect options={options} onChange={action('changed')} {...props}/>
})
