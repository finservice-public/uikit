import React from 'react'
import PropTypes from 'prop-types'
import Icon from 'ui/Icon'
import Block from 'ui/Block'
import _ from 'lodash'
import styled from 'styled-components'
import Input from 'ui/Input'

const InputContainer = styled(Input)`
  & input {
    text-overflow: ellipsis;
  }
`

const Root = React.forwardRef((props, ref) => (
    <InputContainer {...props}/>
))

const ForwardInput = ({
  className, style,
  placeholder, onChange, onFocus,
  isDisable, currentValue, tabIndex, isLoading, options, isFocus,
  normalizer, denormalizer, onKeyDown,
  onClick, htmlID, dataSet,
  mask, maskChar, unmask,
  left, right, prefix, postfix, isAllowClear, onClear,

  label, isFullWidth, errorWidth, error, warn, help, isShowForceValidate,
  onClickLeft, onClickRight, size, isIconDown, onMount, isMarginForHelp
}) => {
  const postfixComponent = (
      <Block>
        {!isLoading && postfix}
        {isLoading && <Icon icon="loading" color="gray4"/>}
        {!_.isEmpty(options) && !isLoading && !postfix && isIconDown &&
        <Icon
            iconType="redesign"
            icon="down"
            rotate={isFocus ? 180 : 0}
            isMarginTop
            marginTop={0.2}
            color={!isDisable ? 'gray4' : 'gray3'}
            size={size === 'lg' ? 1.3 : size === 'sm' ? 1.2 : 1}
        />
        }
      </Block>
  )

  return (
      <Root
          className={className}
          style={style}
          placeholder={placeholder}
          onChange={onChange}
          onFocus={onFocus}
          isDisable={isDisable}
          value={currentValue}
          tabIndex={tabIndex}
          isCommonInput
          isForceChange
          postfix={postfixComponent}
          isAllowClear={isAllowClear}
          normalizer={normalizer}
          denormalizer={denormalizer}
          onKeyDown={onKeyDown}
          onClick={onClick}
          htmlID={htmlID || null}
          dataSet={dataSet}
          mask={mask}
          unmask={unmask}
          maskChar={maskChar}
          left={left}
          right={right}
          prefix={prefix}
          onClickLeft={onClickLeft}
          onClickRight={onClickRight}
          size={size}
          label={label}
          error={error}
          warn={warn}
          help={help}
          isShowForceValidate={isShowForceValidate}
          isFullWidth={isFullWidth}
          errorWidth={errorWidth}
          onClear={onClear}
          isMarginForHelp={isMarginForHelp}
          onMount={onMount}
      />
  )
}

ForwardInput.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  isDisable: PropTypes.bool,
  currentValue: PropTypes.any,
  tabIndex: PropTypes.any,
  isLoading: PropTypes.bool,
  options: PropTypes.array,
  isFocus: PropTypes.bool,
  normalizer: PropTypes.func,
  denormalizer: PropTypes.func,
  onKeyDown: PropTypes.func,
  onClick: PropTypes.func,
  htmlID: PropTypes.any,
  mask: PropTypes.string,
  maskChar: PropTypes.string,
  unmask: PropTypes.func,
  left: PropTypes.any,
  right: PropTypes.any,
  prefix: PropTypes.any,
  postfix: PropTypes.any,
  isAllowClear: PropTypes.bool,
  size: PropTypes.oneOf(['xs', 'sm', 'lg']),

  onClickLeft: PropTypes.func,
  onClickRight: PropTypes.func,

  label: PropTypes.any,
  isFullWidth: PropTypes.bool,
  error: PropTypes.string,
  errorWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  warn: PropTypes.string,
  help: PropTypes.string,
  isShowForceValidate: PropTypes.bool,

  isIconDown: PropTypes.bool,

  onMount: PropTypes.func,

  isMarginForHelp: PropTypes.bool
}

ForwardInput.defaultProps = {
  isIconDown: true,
  isMarginForHelp: false
}

export default React.forwardRef((props, ref) => <ForwardInput {...props}/>)
