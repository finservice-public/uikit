import _ from 'lodash'
import React, { Fragment, useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { isSafe } from 'utils/object'
import { colors } from 'styles'
import { hexToRgba } from 'utils/colors'
import Block from 'ui/Block'
import Paragraph from 'ui/Typography/Paragraph'
import Text from 'ui/Typography/Text'
import SwipeableBottomSheet from 'react-swipeable-bottom-sheet'
import ForwardInput from '../ForwardInput'

const Label = styled(Paragraph)`
  margin-bottom: 0 !important;
  padding: 8px 20px;
  border-bottom: 1px solid ${colors.gray2};
  font-weight: 500;
  line-height: 40px !important;
`

const Item = styled(Paragraph)`
  margin-bottom: ${props => `${props.isLast ? '8px' : 0} !important`};
  padding: 8px 20px;
  border-bottom: ${props => (!props.isLast && !props.isText ? `1px solid ${colors.gray2}` : 'none')};
  transition: background .3s ease-in-out;
  font-weight: ${props => (props.isActive ? 500 : 400)};
  background: ${props => (props.isActive ? colors.gray1 : 'white')};
  line-height: 1.7 !important;
  
  &:hover {
    background: ${props => (!props.isText ? colors.gray1 : 'transparent')};
  }
`

const Input = styled(ForwardInput)`
  & input {
    border-color: ${colors.primary} !important;
    
    &:hover {
      border-color: ${colors.primary} !important;
    }
  }
`

const TouchSelect = ({
  className, style,
  size, options, tabIndex, isDisable, htmlID, placeholder,
  value,
  onChange, onBlur, onFocus,

  inputText, onChangeText, isLoading, isFocus, normalizer, denormalizer,
  mask, maskChar, unmask, left, right, prefix, postfix,
  onClickLeft, onClickRight, dataSet,
  label, isFullWidth, errorWidth, error, warn, help, isShowForceValidate
}) => {
  const isInput = isSafe(inputText) || isSafe(onChangeText)

  const [isOpen, setIsOpen] = useState(false)
  const [rootTarget, setRootTarget] = useState(null)
  const [target, setTarget] = useState(null)

  const handleOpen = () => {
    setIsOpen(true)
    onFocus()
    if (rootTarget) rootTarget.blur()
  }

  const handleClose = (isBlur = true) => {
    setIsOpen(false)
    if (isBlur) onBlur()
    if (target) target.blur()
  }

  const handleChange = option => {
    onChange(option.value)
    handleClose(false)
  }

  const resolveValue = isInput ? inputText : _.get(_.find(options, option => option.value === value), 'label', '')

  return (
    <Fragment>
      <ForwardInput
          className={className}
          style={style}
          placeholder={placeholder}
          onFocus={handleOpen}
          options={options}
          currentValue={resolveValue}
          tabIndex={tabIndex}
          htmlID={htmlID}
          dataSet={dataSet}
          isDisable={isDisable}
          isLoading={isLoading}
          isFocus={isFocus}
          normalizer={normalizer}
          denormalizer={denormalizer}
          mask={mask}
          unmask={unmask}
          maskChar={maskChar}
          left={left}
          right={right}
          prefix={prefix}
          postfix={postfix}
          onClickLeft={onClickLeft}
          onClickRight={onClickRight}
          size={size}
          label={label}
          error={error}
          warn={warn}
          help={help}
          isShowForceValidate={isShowForceValidate}
          isFullWidth={isFullWidth}
          errorWidth={errorWidth}
          onMount={setRootTarget}
          isMarginForHelp
      />

      {!isDisable &&
      <SwipeableBottomSheet
          overflowHeight={0}
          open={isOpen}
          onChange={isOpen ? () => handleClose() : null}
          overlayStyle={isOpen ? { background: hexToRgba(colors.gray6, 0.6), opacity: 1 } : { background: 'transparent' }}
          style={isOpen ? { zIndex: 2000 } : {}}
      >
          <Block
              height="50vh"
              maxWidth="100%"
          >
            {!!label && !isInput && <Label>{label}</Label>}

            {isInput &&
            <Label marginTop={8} isRem={false} marginBottom={8}>
              <Input
                  placeholder={placeholder}
                  onChange={v => {
                    onChangeText(v?.target ? v?.target?.value : v)
                  }}
                  options={options}
                  currentValue={inputText}
                  tabIndex={tabIndex}
                  htmlID={htmlID}
                  dataSet={dataSet}
                  isDisable={isDisable}
                  isLoading={isLoading}
                  isFocus={isFocus}
                  normalizer={normalizer}
                  denormalizer={denormalizer}
                  mask={mask}
                  unmask={unmask}
                  maskChar={maskChar}
                  left={left}
                  right={right}
                  prefix={prefix}
                  postfix={postfix}
                  onClickLeft={onClickLeft}
                  onClickRight={onClickRight}
                  size={size}
                  label={label}
                  isIconDown={false}
                  onMount={setTarget}
              />
            </Label>
            }

            {!_.isEmpty(options) ?
              _.map(options, (option, index) => (
                <Item
                    key={option.value}
                    type="text"
                    isActive={option.value === value}
                    onClick={() => {
                      handleChange(option)
                    }}
                    isLast={index === options.length - 1}
                >
                    {option.node ?? option.label}
                </Item>
              )) :
              <Item isText><Text type="gray4">Данные не найдены</Text></Item>
            }
          </Block>
      </SwipeableBottomSheet>
      }
    </Fragment>
  )
}

TouchSelect.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  size: PropTypes.oneOf(['xs', 'sm', 'lg']),
  value: PropTypes.any,
  tabIndex: PropTypes.any,
  isDisable: PropTypes.bool,
  htmlID: PropTypes.string,
  placeholder: PropTypes.string,
  options: PropTypes.array,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,

  label: PropTypes.any,
  isFullWidth: PropTypes.bool,
  error: PropTypes.string,
  errorWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  warn: PropTypes.string,
  help: PropTypes.string,
  isShowForceValidate: PropTypes.bool,

  inputText: PropTypes.any,
  onChangeText: PropTypes.func,
  isLoading: PropTypes.bool,
  isFocus: PropTypes.bool,
  normalizer: PropTypes.func,
  denormalizer: PropTypes.func,
  mask: PropTypes.string,
  maskChar: PropTypes.string,
  unmask: PropTypes.func,
  left: PropTypes.any,
  right: PropTypes.any,
  prefix: PropTypes.any,
  postfix: PropTypes.any,

  onClickLeft: PropTypes.func,
  onClickRight: PropTypes.func,
  dataSet: PropTypes.object
}

TouchSelect.displayName = 'SelectPicker'

export default TouchSelect
