import _ from 'lodash'
import fp from 'lodash/fp'
import React, { Fragment, PureComponent } from 'react'
import PropTypes from 'prop-types'
import AutoComplete from 'antd/es/auto-complete'
import withMargin from 'ui/common/withMargin'
import escapeStringRegexp from 'escape-string-regexp'
import { Context } from 'ui/Provider/Context'
import styled, { createGlobalStyle } from 'styled-components'
import { safeObject, isSafe } from 'utils/object'
import Block from 'ui/Block'
import { designInputMarginBottom } from 'ui/common/withFormProps/constants'
import { colors } from 'styles'
import TouchSelect from './TouchSelect'
import ForwardInput from './ForwardInput'

const RootAutocomplete = styled(AutoComplete)`
  & .ant-select-selection {
    background: transparent !important;
  }
  
  & .ant-input[disabled] {
    background-color: ${colors.gray1} !important;
  }
  
  &:hover .ant-input[disabled] {
    background-color: ${colors.gray1} !important;
  } 
`

const AntSelectOverride = createGlobalStyle`
  .ant-select-dropdown-menu-vertical {
    width: 100% !important;
  }
`

const Option = AutoComplete.Option

class InputSelect extends PureComponent {
  static contextType = Context

  static propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,
    htmlID: PropTypes.any,
    dataSet: PropTypes.object,

    value: PropTypes.any,
    inputValue: PropTypes.string,
    placeholder: PropTypes.string,
    options: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.shape({
        value: PropTypes.any,
        label: PropTypes.any,
        node: PropTypes.any
      })),
      PropTypes.arrayOf(PropTypes.string)
    ]),
    isDisable: PropTypes.bool,
    size: PropTypes.oneOf(['xs', 'sm', 'lg']),
    tabIndex: PropTypes.any,
    isLoading: PropTypes.bool,
    isForceOpen: PropTypes.bool,
    normalizer: PropTypes.func,
    denormalizer: PropTypes.func,
    isAdaptive: PropTypes.bool,
    isFilterOption: PropTypes.bool,
    isSelect: PropTypes.bool,
    dropdownStyle: PropTypes.object,
    mask: PropTypes.string,
    maskChar: PropTypes.string,
    unmask: PropTypes.func,
    left: PropTypes.any,
    right: PropTypes.any,
    prefix: PropTypes.any,
    postfix: PropTypes.any,
    isAllowClear: PropTypes.bool,

    onChange: PropTypes.func,
    onChangeText: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onClickLeft: PropTypes.func,
    onClickRight: PropTypes.func,

    label: PropTypes.any,
    isFullWidth: PropTypes.bool,
    error: PropTypes.string,
    errorWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    warn: PropTypes.string,
    help: PropTypes.string,
    isShowForceValidate: PropTypes.bool,

    isClearInputBlur: PropTypes.bool
  }

  static defaultProps = {
    value: null,
    placeholder: '',
    isDisable: false,
    size: 'lg',
    tabIndex: null,
    isLoading: false,
    isForceOpen: false,
    options: [],
    isAdaptive: true,
    isFilterOption: true,
    isSelect: true,
    mask: '',
    maskChar: '_',
    unmask: null,
    left: null,
    right: null,
    prefix: null,
    postfix: null,
    isAllowClear: false,

    label: null,
    isFullWidth: true,
    errorWidth: null,
    error: null,
    warn: null,
    help: null,
    isShowForceValidate: false,

    isClearInputBlur: true
  }

  constructor(props) {
    super(props)

    const { isSelect, isClearInputBlur } = props

    const state = {
      value: props.value || null,
      isFocus: false
    }

    const options = _.isObject(_.get(props.options, '0')) ? props.options : _.map(props.options, o => ({ value: o, label: 0 }))
    if (isSelect && isClearInputBlur) {
      state.inputValue = props.value && !_.isEmpty(options) ?
        _.get(_.find(options, option => String(option.value) === String(props.value)), 'label', '') : ''
    } else if (isSelect && !isClearInputBlur) {
      state.inputValue = props.inputValue
    } else {
      state.inputValue = ''
    }
    this.state = state
  }

  componentDidUpdate() {
    const { isSelect, isClearInputBlur, value } = this.props

    const state = {}

    if (isSelect && this.props.value !== this.state.value && !this.state.isFocus && !_.isEmpty(this.resolveOptions())) {
      state.value = this.props.value
      const inputValue = !isClearInputBlur && value ? this.state.inputValue : ''
      state.inputValue = isClearInputBlur ?
        _.get(_.find(this.resolveOptions(), option => String(option.value) === String(this.props.value)), 'label', '') ||
          inputValue : inputValue
    } else if (isSelect && !isClearInputBlur && this.props.inputValue !== this.state.inputValue) {
      state.inputValue = this.props.inputValue
    }
    if (!isSelect) {
      state.value = this.props.value
      state.inputValue = this.props.value
    }

    this.setState(state)
  }

  render() {
    const {
      className, style, htmlID, dataSet,

      placeholder, isDisable, size, tabIndex,
      isLoading, isSelect,

      normalizer, denormalizer, dropdownStyle,
      mask, maskChar, unmask, isForm,
      left, right, prefix, postfix, isAllowClear,

      label, error, warn, help, isShowForceValidate, isFullWidth, errorWidth,
      onClickLeft, onClickRight,

      isClearInputBlur
    } = this.props
    const { value, inputValue, isFocus } = this.state

    return (
      <Fragment>
        <AntSelectOverride/>
        {!this.isTouchSelect() ?
          <RootAutocomplete
            className={className}
            style={safeObject({
              width: '100%',
              ...style,
              marginBottom: style.marginBottom ? style.marginBottom :
                isForm || error || warn || help || label ? `${designInputMarginBottom}px` : null
            })}
            onSelect={this.handleChange()}
            onChange={this.handleChangeText}
            onFocus={this.handleFocus}
            onBlur={() => this.handleBlur()}
            disabled={isDisable}
            open={isFocus}
            {...safeObject({
              value: !isSelect ? !_.isObject(value) ? value : '' :
                isFocus || !isClearInputBlur ? (inputValue ? `${inputValue}` : '') : inputValue,
              size: size === 'lg' ? 'large' : size === 'xs' ? 'small' : null,
              dropdownMenuStyle: dropdownStyle || null,
              tabIndex
            })}
            dataSource={_.map(this.resolveOptions(), (option, index) => (
              <Option key={option.value} text={option.label}>
                <Block
                    {...safeObject({
                      htmlID: htmlID ? `${htmlID}-${index}` : null
                    })}
                >
                  {option.node || option.label}
                </Block>
              </Option>
            ))}
            optionLabelProp="text"
            filterOption={null}
          >
            <ForwardInput
              placeholder={placeholder}
              onChange={this.handleChangeText}
              onFocus={this.handleFocus}
              options={this.resolveOptions()}
              currentValue={isSelect ? inputValue : !_.isObject(value) ? value : ''}
              tabIndex={tabIndex}
              htmlID={htmlID}
              dataSet={dataSet}
              isDisable={isDisable}
              isLoading={isLoading}
              onClick={this.handleClick}
              onKeyDown={this.handleKeyDown}
              isFocus={isFocus}
              normalizer={normalizer}
              denormalizer={denormalizer}
              mask={mask}
              unmask={unmask}
              maskChar={maskChar}
              left={left}
              right={right}
              prefix={prefix}
              postfix={postfix}
              isAllowClear={isAllowClear}
              onClickLeft={onClickLeft}
              onClickRight={onClickRight}
              size={size}
              label={label}
              error={error}
              warn={warn}
              help={help}
              isShowForceValidate={isShowForceValidate}
              isFullWidth={isFullWidth}
              errorWidth={errorWidth}
              onClear={this.handleClear}
            />
          </RootAutocomplete> :
          <TouchSelect
              style={style}
              htmlID={htmlID}
              isDisable={isDisable}
              tabIndex={tabIndex}
              value={value}
              onChange={v => this.handleChange(true, true, true)(v)}
              options={this.resolveOptions()}
              onFocus={this.handleFocus}
              onBlur={() => this.handleBlur()}
              size={size}
              placeholder={placeholder}
              label={label}
              error={error}
              warn={warn}
              help={help}
              isShowForceValidate={isShowForceValidate}
              isFullWidth={isFullWidth}
              errorWidth={errorWidth}
              inputText={isSelect ? inputValue : !_.isObject(value) ? value : ''}
              onChangeText={this.handleChangeText}
              isLoading={isLoading}
              isFocus={isFocus}
              normalizer={normalizer}
              denormalizer={denormalizer}
              mask={mask}
              maskChar={maskChar}
              unmask={unmask}
              left={left}
              right={right}
              prefix={prefix}
              postfix={postfix}
              onClickLeft={onClickLeft}
              onClickRight={onClickRight}
              dataSet={dataSet}
          />
        }
      </Fragment>
    )
  }

  isTouchSelect = () => this.context.responsiveModel.isTouch && this.props.isAdaptive

  resolveOptions() {
    const { options, isSelect } = this.props
    const { inputValue, value } = this.state
    let resolve = []

    if (isSelect) resolve = options
    else {
      const isSimpleOptions = _.isObject(_.get(options, '0'))
      resolve = isSimpleOptions ? fp.map(o => ({ value: o.label, label: o.label, node: o.node }))(options) :
        fp.map(o => ({ value: o, label: o }))(options)
    }

    const search = isSelect ? inputValue : !_.isObject(value) ? value : ''
    if (this.props.isFilterOption && isSafe(search) && `${search}`.trim() !== '') {
      resolve = _.filter(resolve, r => {
        const filterRegExpString = escapeStringRegexp(`${search}`.trim())
        if (filterRegExpString !== '') {
          return new RegExp(`${filterRegExpString}+`, 'ig').test(r.label.toLowerCase()
            .trim())
        }
        return true
      })
    }

    return resolve
  }

  handleChange = (isChange = true, isSelectOption = true, isAfterBlur = false) => v => {
    const { onChange, isClearInputBlur } = this.props

    if (!this.state.value && !v) return

    const findLabel = _.get(_.find(this.resolveOptions(), option => String(option.value) === String(v)), 'label', '')
    const inputValue = findLabel ?? ''

    const state = {
      value: v
    }
    if (isClearInputBlur) state.inputValue = inputValue
    if (isSelectOption) state.isFocus = false

    this.setState(state)
    if (isAfterBlur) this.handleBlur(v)

    if (onChange) onChange(v)
  }

  handleChangeInputValue = v => {
    this.setState({ inputValue: v })
  }

  handleChangeText = inputValue => {
    const { onChange, onChangeText, normalizer, isSelect } = this.props

    if (normalizer && !normalizer(inputValue)) return

    const option = _.find(this.resolveOptions(), p => p.label === inputValue) ||
        _.find(this.resolveOptions(), p => p.value === inputValue) || null

    if (option) {
      this.handleChange(true, false)(option.value)
    } else {
      if (onChangeText) onChangeText(inputValue)
      if (!isSelect && onChange) onChange(inputValue)
      this.setState(safeObject({
        inputValue,
        value: !isSelect ? inputValue : null
      }))
    }
  }

  handleClear = () => {
    const { onChange, onChangeText } = this.props
    this.setState({
      value: null,
      inputValue: ''
    })
    if (onChange) onChange(null)
    if (onChangeText) onChangeText('')
  }

  handleKeyDown = e => {
    if ((e.keyCode === 40 || e.keyCode === 13 || this.props.isForceOpen) && !this.state.isFocus) {
      this.setState({
        isFocus: true
      })
    }
  }

  handleClick = () => {
    this.setState({
      isFocus: true
    })
  }

  handleFocus = () => {
    const { onFocus } = this.props

    this.setState({
      isFocus: true
    })

    if (onFocus) onFocus()
  }

  handleBlur = (currentValue) => {
    const { onBlur, isSelect, isClearInputBlur } = this.props
    const { inputValue } = this.state

    this.setState({
      isFocus: false
    })

    if (isSelect && isClearInputBlur) {
      const value = !isSafe(currentValue) ? _.get(_.find(this.resolveOptions(), option => String(option.label) === String(inputValue)), 'value', '') :
        currentValue
      if (value) {
        this.handleChange()(value)
      } else {
        this.setState({
          inputValue: ''
        }, () => {
          this.handleChange(false)(null)
        })
      }
    }

    if (onBlur) onBlur()
  }
}

InputSelect.displayName = 'InputSelect'

export default withMargin({
  displayName: 'InputSelect'
})(InputSelect)
