import _ from 'lodash'
import React from 'react'
import { storiesOf } from '@storybook/react'
import Button from 'ui/Button'
import { Text } from 'ui/Typography'
import Description from './Description'

const stories = storiesOf('Description', module)

stories.addDecorator(story => (
  <div style={{ width: '80%' }}>
    {story()}
  </div>
))

const options = [
  { id: 1, label: 'label', span: 2, node: <Text>node</Text> },
  { id: 2, label: <Text isStrong>title</Text>, node: <Button>Button</Button> }
]

stories
  .add('default', () => (
    <Description
      options={options}
    />
  ))
  .add('1 in row', () => (
    <Description
      column={1}
      options={options}
    />
  ))
  .add('isBordered', () => (
    <Description
      isBordered
      column={1}
      options={options}
    />
  ))
  .add('sm', () => (
    <Description
      isBordered
      size="sm"
      column={1}
      options={options}
    />
  ))
  .add('xs', () => (
    <Description
      isBordered
      size="xs"
      column={1}
      options={options}
    />
  ))
  .add('with Title', () => (
    <Description
      title="Title"
      isBordered
      size="xs"
      column={1}
      options={options}
    />
  ))
