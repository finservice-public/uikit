import _ from 'lodash'
import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import withMargin from 'ui/common/withMargin'
import Loader from 'ui/Loader'
import styled from 'styled-components'
import { colors } from 'styles'
import { Context } from 'ui/Provider/Context'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject, isSafe } from 'utils/object'
import { r2px } from 'utils/style'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isClick')
  _.unset(resolveProps, 'cursor')
  _.unset(resolveProps, 'isFlex')
  _.unset(resolveProps, 'isInline')
  _.unset(resolveProps, 'isLoading')
  return <div {...resolveProps}/>
})`
  position: relative;
  cursor: ${props => (!props.cursor ? props.isClick ? 'pointer' : 'inherit' : props.cursor)};
  display: ${props => (props.isFlex ? !props.isInline ? 'flex' : 'inline-flex' : !props.isInline ? 'block' : 'inline-block')};
  opacity: ${props => (props.isLoading ? 0.5 : 1)};
`

const LoaderContainer = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 1;
  display: flex;
  justify-content: center;
  padding-top: 5rem;
`

const Block = ({
  style, className, htmlID, dataSet,
  horizontalPlacement, verticalPlacement, isCenter, isRem,
  padding, paddingLeft, paddingRight, paddingTop, paddingBottom,
  cursor, color, background, borderWidth, borderColor, borderRadius, isInline, isLoading,

  maxWidth, minWidth, maxHeight, minHeight, flex, isWrap,

  onClick, onMouseOver, onMouseLeave, onMouseEnter,

  children
}) => {
  const isFlex = horizontalPlacement !== 'none' || verticalPlacement !== 'none' || isCenter

  const applyPlacement = placement => {
    switch (placement) {
      case 'none':
        return 'flex-start'
      case 'left':
        return 'flex-start'
      case 'right':
        return 'flex-end'
      case 'center':
        return 'center'
      case 'top':
        return 'flex-start'
      case 'bottom':
        return 'flex-end'
      case 'between':
        return 'space-between'
      case 'around':
        return 'space-around'
      default:
        return 'initial'
    }
  }

  const { remPoint } = useContext(Context)
  const point = isRem ? remPoint : 1

  return (
    <Root
      className={className}
      onClick={onClick}
      isClick={!!onClick}
      cursor={cursor}
      isFlex={isFlex}
      isInline={isInline}
      onMouseOver={onMouseOver}
      onMouseLeave={onMouseLeave}
      onMouseEnter={onMouseEnter}
      isLoading={isLoading}
      style={{
        maxWidth: maxWidth ? _.isNumber(maxWidth) ? `${isRem ? maxWidth * point : maxWidth}px` : maxWidth : null,
        minWidth: minWidth ? _.isNumber(minWidth) ? `${isRem ? minWidth * point : minWidth}px` : minWidth : null,
        maxHeight: maxHeight ? _.isNumber(maxHeight) ? `${isRem ? maxHeight * point : maxHeight}px` : maxHeight : null,
        minHeight: minHeight ? _.isNumber(minHeight) ? `${isRem ? minHeight * point : minHeight}px` : minHeight : null,
        ...style,
        ...safeObject({
          justifyContent: isFlex ? !isCenter ? applyPlacement(horizontalPlacement) : 'center' : null,
          alignItems: isFlex ? !isCenter ? applyPlacement(verticalPlacement) : 'center' : null,
          flexWrap: isFlex && isWrap ? 'wrap' : null,
          padding: padding ? r2px(padding, point) : null,
          paddingLeft: paddingLeft && !padding ? r2px(paddingLeft, point) : null,
          paddingRight: paddingRight && !padding ? r2px(paddingRight, point) : null,
          paddingTop: paddingTop && !padding ? r2px(paddingTop, point) : null,
          paddingBottom: paddingBottom && !padding ? r2px(paddingBottom, point) : null,
          color: color ? colors[color] ? colors[color] : color : null,
          background: background ? colors[background] ? colors[background] : background : null,
          border: borderWidth ? `${borderWidth * point}px solid ${borderColor ? colors[borderColor] || borderColor : colors.black}` : null,
          borderRadius: borderRadius ? _.isNumber(borderRadius) ? `${borderRadius * point}px` : borderRadius : null,
          cursor: isLoading ? 'not-allowed' : null,
          flex: isSafe(flex) ? flex : null
        })
      }}
      {...safeObject({
        id: htmlID || null
      })}
      {...applyHtmlData(dataSet)}
    >
      {children}
      {isLoading &&
      <LoaderContainer>
        <Loader />
      </LoaderContainer>
      }
    </Root>
  )
}

Block.propTypes = {
  style: PropTypes.object,
  className: PropTypes.string,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  horizontalPlacement: PropTypes.oneOf(['left', 'center', 'right', 'between', 'around', 'none']),
  verticalPlacement: PropTypes.oneOf(['top', 'center', 'bottom', 'none']),
  isCenter: PropTypes.bool,
  padding: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  paddingLeft: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  paddingRight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  paddingTop: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  paddingBottom: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  color: PropTypes.string, // colors of theme or custom color/background
  background: PropTypes.string, // colors of theme or custom color/background
  borderWidth: PropTypes.number,
  borderColor: PropTypes.string, // colors of theme or custom color/background
  borderRadius: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  isRem: PropTypes.bool,
  cursor: PropTypes.string,
  isInline: PropTypes.bool,
  isLoading: PropTypes.bool,

  maxWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  minWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  maxHeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  minHeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  flex: PropTypes.number,
  isWrap: PropTypes.bool,

  onClick: PropTypes.func,
  onMouseOver: PropTypes.func,
  onMouseLeave: PropTypes.func,
  onMouseEnter: PropTypes.func
}

Block.defaultProps = {
  horizontalPlacement: 'none',
  verticalPlacement: 'none',
  padding: 0,
  paddingLeft: 0,
  paddingRight: 0,
  paddingTop: 0,
  paddingBottom: 0,
  isCenter: false,
  color: '',
  background: '',
  borderWidth: 0,
  borderColor: '',
  borderRadius: 0,
  isRem: true,
  cursor: '',
  isInline: false,
  isLoading: false,
  isWrap: false
}

Block.displayName = 'Block'

export default withMargin({ displayName: 'Block' })(Block)
