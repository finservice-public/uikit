import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import { Paragraph } from 'ui/Typography'
import { hexToRgba } from 'utils/colors'
import { colors } from 'styles'
import Block from './Block'

const stories = storiesOf('Block', module)

stories.addDecorator(story => (
  <div style={{ width: '40rem' }}>
    {story()}
  </div>
))

const content = (
  <Paragraph>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias amet
  </Paragraph>
)

stories
  .add('with background', () => (
    <Block
      background="secondary"
      height={20}
    >
      {content}
    </Block>
  ))
  .add('all center', () => (
    <Block
      background="secondary"
      isCenter
      height={20}
    >
      {content}
    </Block>
  ))
  .add('horizontal placement', () => (
    <Block
      background="secondary"
      horizontalPlacement="right"
      height={20}
    >
      {content}
    </Block>
  ))
  .add('horizontal & vertical placement', () => (
    <Fragment>
      <Block
        background="secondary"
        horizontalPlacement="right"
        verticalPlacement="bottom"
        height={20}
        isMarginBottom
      >
        {content}
      </Block>
      <Block
        background="secondary"
        horizontalPlacement="center"
        verticalPlacement="bottom"
        height={20}
        isMarginBottom
      >
        {content}
      </Block>
    </Fragment>
  ))
  .add('with all padding', () => (
    <Block
      padding={2}
      background="secondary"
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum deleniti dignissimos dolor doloremque dolores ducimus est iste magnam magni modi molestias nihil officiis placeat quia, quis quod reiciendis similique temporibus, tenetur vel? Architecto deserunt, fuga. Est impedit incidunt praesentium quos sunt? Ab adipisci animi aspernatur atque consequatur dolor dolores enim error exercitationem fuga iusto laudantium magnam, necessitatibus nemo non, nulla obcaecati odit perferendis quaerat qui sunt, velit voluptate. A iusto magnam maiores necessitatibus obcaecati? Aliquid architecto atque blanditiis culpa doloremque ea eligendi hic minus natus obcaecati optio, placeat quam quibusdam quod, tenetur voluptas voluptatibus. Inventore molestiae mollitia qui sint vero!
    </Block>
  ))
  .add('with single padding', () => (
    <Block
      paddingLeft={1}
      paddingRight={4}
      paddingTop={1}
      paddingBottom={2}
      background="secondary"
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum deleniti dignissimos dolor doloremque dolores ducimus est iste magnam magni modi molestias nihil officiis placeat quia, quis quod reiciendis similique temporibus, tenetur vel? Architecto deserunt, fuga. Est impedit incidunt praesentium quos sunt? Ab adipisci animi aspernatur atque consequatur dolor dolores enim error exercitationem fuga iusto laudantium magnam, necessitatibus nemo non, nulla obcaecati odit perferendis quaerat qui sunt, velit voluptate. A iusto magnam maiores necessitatibus obcaecati? Aliquid architecto atque blanditiis culpa doloremque ea eligendi hic minus natus obcaecati optio, placeat quam quibusdam quod, tenetur voluptas voluptatibus. Inventore molestiae mollitia qui sint vero!
    </Block>
  ))
  .add('with background & text color', () => (
    <Block
      padding={2}
      color="white"
      background="secondary"
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum deleniti dignissimos dolor doloremque dolores ducimus est iste magnam magni modi molestias nihil officiis placeat quia, quis quod reiciendis similique temporibus, tenetur vel? Architecto deserunt, fuga. Est impedit incidunt praesentium quos sunt? Ab adipisci animi aspernatur atque consequatur dolor dolores enim error exercitationem fuga iusto laudantium magnam, necessitatibus nemo non, nulla obcaecati odit perferendis quaerat qui sunt, velit voluptate. A iusto magnam maiores necessitatibus obcaecati? Aliquid architecto atque blanditiis culpa doloremque ea eligendi hic minus natus obcaecati optio, placeat quam quibusdam quod, tenetur voluptas voluptatibus. Inventore molestiae mollitia qui sint vero!
    </Block>
  ))
  .add('with border && isLoading', () => (
    <Block
      padding={2}
      background={hexToRgba(colors.primary, 0.1)}
      borderWidth={0.1}
      borderColor={hexToRgba(colors.primary, 0.2)}
      borderRadius={2}
      isLoading
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum deleniti dignissimos dolor doloremque dolores ducimus est iste magnam magni modi molestias nihil officiis placeat quia, quis quod reiciendis similique temporibus, tenetur vel? Architecto deserunt, fuga. Est impedit incidunt praesentium quos sunt? Ab adipisci animi aspernatur atque consequatur dolor dolores enim error exercitationem fuga iusto laudantium magnam, necessitatibus nemo non, nulla obcaecati odit perferendis quaerat qui sunt, velit voluptate. A iusto magnam maiores necessitatibus obcaecati? Aliquid architecto atque blanditiis culpa doloremque ea eligendi hic minus natus obcaecati optio, placeat quam quibusdam quod, tenetur voluptas voluptatibus. Inventore molestiae mollitia qui sint vero!
    </Block>
  ))
