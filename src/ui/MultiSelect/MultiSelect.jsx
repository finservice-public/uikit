import _ from 'lodash'
import Block from 'ui/Block'
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Select from 'antd/lib/select'
import withFormProps from 'ui/common/withFormProps'
import withMargin from 'ui/common/withMargin'
import Loader from 'ui/Loader'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const { Option } = Select

class MultiSelect extends PureComponent {
  constructor(props) {
    super(props)
    this.handleSearch = _.debounce(this.handleSearch, 450)
    this.state = {
      isLoading: false,
      value: this.props.value
    }
  }

  static propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,
    htmlID: PropTypes.any,
    dataSet: PropTypes.object,

    size: PropTypes.oneOf(['xs', 'sm', 'lg']),
    placeholder: PropTypes.string,
    value: PropTypes.array,
    options: PropTypes.arrayOf(PropTypes.shape({
      key: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      label: PropTypes.any
    })),
    isDisable: PropTypes.bool,
    isLoading: PropTypes.bool,

    onChange: PropTypes.func,
    onSearch: PropTypes.func,

    // withFormProps
    onChangeIsInput: PropTypes.func,
    onChangeIsFocus: PropTypes.func,
    tokenSeparators: PropTypes.array
  }

  static defaultProps = {
    size: 'lg',
    placeholder: '',
    value: null,
    isDisable: false,
    isLoading: false,
    tokenSeparators: [',']
  }

  render() {
    const {
      className, style, htmlID, dataSet,
      size, placeholder, value, onSearch, isDisable,
      allowSelectAll, selectAllLabel, selectAllKey, selectAllValue,
      onChangeIsFocus, tokenSeparators
    } = this.props
    const options = this.getOptions()
    const isLoading = this.state.isLoading || this.props.isLoading

    return (
        <Select
            className={className}
            style={{ width: '100%', style }}
            mode="multiple"
            tokenSeparators={tokenSeparators}
            placeholder={placeholder}
            onChange={this.handleChange}
            allowClear
            disabled={isDisable}
            loading={isLoading}
            {...safeObject({
              value: allowSelectAll ? this.state.value : value,
              onSearch: onSearch ? this.handleSearch : null,
              labelInValue: !!onSearch || null,
              filterOption: onSearch ? false : null,
              notFoundContent: isLoading ? <Loader/> : null,
              size: size === 'lg' ? 'large' : size === 'xs' ? 'small' : null,
              id: htmlID || null
            })}
            onFocus={() => {
              if (onChangeIsFocus) onChangeIsFocus(true)
            }}
            onBlur={() => {
              if (onChangeIsFocus) onChangeIsFocus(false)
            }}
            {...applyHtmlData(dataSet)}
        >
          {allowSelectAll && !isLoading && <Option
            key={selectAllKey}
            value={selectAllValue}
          >
            {selectAllLabel}
            <hr/>
          </Option>}
          {_.map(!isLoading ? options : [], (o, index) => (
              <Option
                  key={o.key.toString()}
                  className={this.getOptionClassname(o.key.toString())}
              >
                <Block {...safeObject({ htmlID: htmlID ? `${htmlID}-${index}` : null })}>{o.label}</Block>
              </Option>
          ))}
        </Select>
    )
  }

  handleChange = v => {
    const { onChange, allowSelectAll, selectAllKey, onChangeIsInput } = this.props
    if (allowSelectAll) {
      const options = this.getOptions()
      if (v.find(i => i.key === selectAllKey)) {
        if (v.length > options.length) {
          this.setValues([])
        } else {
          this.setValues([...options])
        }
        return
      }
      this.setValues(v)
      return
    }
    if (onChange) onChange(v)
    if (onChangeIsInput) onChangeIsInput(true)
  }

  setValues = (v) => {
    this.props.onChange(v)
    this.setState({
      value: v
    })
  }

  handleSearch = async v => {
    const { onSearch, onChangeIsInput } = this.props
    if (onSearch) {
      try {
        this.setState({ isLoading: true })
        await onSearch(v)
      } finally {
        this.setState({ isLoading: false })
      }
    }
    if (onChangeIsInput) onChangeIsInput(true)
  }

  getOptions() {
    return _.map(this.props.options, o => ({
      key: o.key.toString(),
      label: o.label
    }))
  }

  getOptionClassname = (key) => {
    const { allowSelectAll } = this.props
    if (!allowSelectAll) return
    return this.state.value && this.state.value.find(v => v.key === key) ? 'ant-select-dropdown-menu-item-selected' : ''
  }
}

MultiSelect.displayName = 'MultiSelect'

export default withMargin({ displayName: 'MultiSelect' })(
  withFormProps({ displayName: 'MultiSelect' })(MultiSelect)
)
