import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import storiesHelpers from 'ui/common/withFormProps/storiesHelpers'
import MultiSelect from './MultiSelect'

const stories = storiesOf('MultiSelect', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

stories
  .add('default', () => (
    <MultiSelect
        options={[
          { key: 1, label: 'One' },
          { key: 2, label: 'Two' },
          { key: 3, label: 'Three' },
          { key: 4, label: 'Four' },
          { key: 5, label: 'Five' }
        ]}
        onChange={action('changed')}
    />
  ))
  .add('with placeholder', () => (
    <MultiSelect
        placeholder="Select"
        options={[
          { key: 1, label: 'One' },
          { key: 2, label: 'Two' },
          { key: 3, label: 'Three' },
          { key: 4, label: 'Four' },
          { key: 5, label: 'Five' }
        ]}
        onChange={action('changed')}
    />
  ))
  .add('async', () => (
    <MultiSelect
        options={[
          { key: 1, label: 'One' },
          { key: 2, label: 'Two' },
          { key: 3, label: 'Three' },
          { key: 4, label: 'Four' },
          { key: 5, label: 'Five' }
        ]}
        onChange={action('changed')}
        onSearch={async () => {
          await new Promise(resolve => {
            setTimeout(() => {
              resolve()
            }, 2000)
          })
        }}
    />
  ))
  .add('selectAll', () => (
    <MultiSelect
        options={[
          { key: 1, label: 'One' },
          { key: 2, label: 'Two' },
          { key: 3, label: 'Three' },
          { key: 4, label: 'Four' },
          { key: 5, label: 'Five' }
        ]}
        onChange={action('changed')}
        onSearch={async () => {
          await new Promise(resolve => {
            setTimeout(() => {
              resolve()
            }, 2000)
          })
        }}
        allowSelectAll
        selectAllLabel="Выбрать все"
        selectAllKey="all"
        selectAllValue="all"
    />
  ))
  .add('disabled', () => (
    <MultiSelect
      options={[
        { key: 1, label: 'One' },
        { key: 2, label: 'Two' },
        { key: 3, label: 'Three' },
        { key: 4, label: 'Four' },
        { key: 5, label: 'Five' }
      ]}
      onChange={action('changed')}
      isDisable
    />
  ))
  .add('with htmlID', () => (
    <MultiSelect
        htmlID="select-example"
        options={[
          { key: 1, label: 'One' },
          { key: 2, label: 'Two' },
          { key: 3, label: 'Three' },
          { key: 4, label: 'Four' },
          { key: 5, label: 'Five' }
        ]}
        onChange={action('changed')}
    />
  ))

storiesHelpers({
  stories,
  Component: props => (
      <MultiSelect
          options={[
            { key: 1, label: 'One' },
            { key: 2, label: 'Two' },
            { key: 3, label: 'Three' },
            { key: 4, label: 'Four' },
            { key: 5, label: 'Five' }
          ]}
          onChange={action('changed')}
          {...props}
      />
  )
})
