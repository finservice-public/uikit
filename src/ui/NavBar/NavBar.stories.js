import React, { useState } from 'react'
import { storiesOf } from '@storybook/react'
import NavBar from './NavBar'

const stories = storiesOf('NavBar', module)

stories.addDecorator(story => (
  <div style={{ width: '40rem' }}>
    {story()}
  </div>
))

const options = [
  { value: '1', label: 'One' },
  { value: '2', label: 'Two' },
  { value: '3', label: 'Three' }
]

stories
  .add('default', () => {
    const [value, setValue] = useState('1')

    return (
        <NavBar
            options={options}
            activeKey={value}
            onChange={setValue}
        >
          <div>
            <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium, suscipit.</div>
            <div>Commodi neque praesentium quaerat quis quod, similique ullam ut voluptatem?</div>
            <div>Dolores et expedita, natus neque perferendis quasi recusandae totam voluptates.</div>
            <div>A ab assumenda blanditiis distinctio et, expedita necessitatibus odio quos.</div>
            <div>A, in itaque neque officiis praesentium quis soluta totam voluptates?</div>
            <div>Corporis dolor eum maxime molestiae molestias numquam, possimus sed ut.</div>
            <div>Dignissimos distinctio dolor eum labore magni mollitia odio rem sunt.</div>
            <div>Accusantium autem blanditiis consequatur dignissimos ducimus nostrum odio quasi? Repudiandae!</div>
            <div>Asperiores blanditiis iste minima nihil nisi numquam quo ut voluptate?</div>
            <div>Culpa fuga praesentium reiciendis reprehenderit. Amet dignissimos velit veritatis vero.</div>
          </div>
        </NavBar>
    )
  })
