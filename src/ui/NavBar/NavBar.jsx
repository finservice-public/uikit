import _ from 'lodash'
import React, { useState, useEffect, Fragment, useContext } from 'react'
import PropTypes from 'prop-types'
import Block from 'ui/Block'
import Icon from 'ui/Icon'
import styled from 'styled-components'
import { colors } from 'styles'
import withMargin from 'ui/common/withMargin'
import { applyPolyfillCss } from 'utils/style'
import { Desktop, TabletAndPhone } from 'ui/Responsive'
import Tabs from 'antd/es/tabs'
import Loader from 'ui/Loader'
import { Context } from 'ui/Provider/Context'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject, isSafe } from 'utils/object'

const { TabPane } = Tabs

const RootTabs = styled(Tabs)`
  & .ant-tabs-bar {
    border-bottom: none;
  }
  
  & .ant-tabs-ink-bar {
    height: 0;
    background: transparent;
  }

  & .ant-tabs-ink-bar::after {
    content: "";
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    height: 0;
    width: 0;
  }
  
  & .ant-tabs-tab {
    color: ${props => colors.darkBlue};
    font-weight: 500;
  }
  
  & .ant-tabs-tab-active {
    font-size: 19px;
    color: #000 !important;
  }
  
  & .ant-tabs-nav .ant-tabs-tab {
    margin-right: 0;
    padding: 25px 8px;
  }
  
  & .ant-tabs-tab-prev-icon {
    transform: translate(-50%, -34%);
  }
  
  & .ant-tabs-tab-prev-icon-target {
    font-size: 16px;
    transform: none;
  }
  
  & .ant-tabs-tab-next-icon-target {
    font-size: 16px;
    transform: none;
  }
  
  & .ant-tabs-tab-next-icon {
    transform: translate(-50%, -34%);
  }
`

const Content = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isTabletAndPhone')
  return <Block {...resolveProps}/>
})`
  ${applyPolyfillCss('width', 'calc(100% - 280px)')}
  border-left: ${props => (!props.isTabletAndPhone ? `${colors.gray2} solid 1px` : 'none')};
`

const Item = styled(Block)`
  transition: font-size .175s ease-in-out;
  font-weight: 500;
  user-select: none;
`

const NavBar = ({
  className, style, htmlID, dataSet,

  isFullWidth, options, children,
  theme, defaultActiveKey,

  contentName,

  onChange, ...props
}) => {
  const [activeKey, setActiveKey] = useState(null)
  const { responsiveModel } = useContext(Context)

  const handleChange = value => () => {
    if (onChange) {
      onChange(value)
    } else {
      setActiveKey(value)
    }
  }

  useEffect(() => {
    if (!activeKey && defaultActiveKey) {
      setActiveKey(defaultActiveKey)
    } else if (isSafe(props.activeKey)) {
      setActiveKey(props.activeKey)
    }
  }, [props.activeKey])

  const { isTabletAndPhone } = responsiveModel

  return (
    <Fragment>
      <Desktop>
        <Block
          className={className}
          style={safeObject({
            width: isFullWidth ? '100%' : _.get(style, 'width', null),
            ...style
          })}
          horizontalPlacement={!isTabletAndPhone ? 'left' : 'none'}
          {...safeObject({
            id: htmlID || null
          })}
          {...applyHtmlData(dataSet)}
        >
          <Block width={24} isMarginBottom marginBottom={isTabletAndPhone ? 3 : 0}>
            {_.map(options, (option, index) => (
              <Item
                key={option.value}
                color={(() => {
                  if (option.isDisable) return 'secondary'

                  return activeKey !== option.value ? colors.darkBlue : 'text'
                })()}
                paddingRight={3}
                isMarginBottom={index !== options.length - 1}
                onClick={() => {
                  if (!option.isDisable && !option.isLoading && option.value !== activeKey) {
                    handleChange(option.value)()
                  }
                }}
                style={{
                  fontSize: activeKey === option.value ? '20px' : '16px'
                }}
                cursor={!option.isDisable ? 'pointer !important' : 'not-allowed !important'}
                htmlID={htmlID ? `${htmlID}-${option.value}` : null}
                {...safeObject({
                  id: htmlID ? `${htmlID}-${option.value}` : null
                })}
              >
                <Block horizontalPlacement="left" verticalPlacement="center">
                  <Block isMarginRight>{option.label}</Block>
                  <Block>
                    {(() => {
                      if (option.isLoading) {
                        return (
                          <Loader size={1} isColor={false}/>
                        )
                      }

                      if (option.isSuccess && activeKey !== option.value) {
                        return (
                          <Icon
                            iconType="design"
                            icon="check"
                            color={!option.isDisable ? 'success' : '' +
                                'gray3'}
                            size={1}
                          />
                        )
                      }

                      if (option.isError && activeKey !== option.value) {
                        return (
                          <Icon
                            iconType="design"
                            icon="close"
                            color={!option.isDisable ? 'errorLight' : 'gray3'}
                            size={1}
                          />
                        )
                      }

                      return null
                    })()}
                  </Block>
                </Block>
              </Item>
            ))}
          </Block>
          <Content
            paddingLeft={!isTabletAndPhone ? 3 : 0}
            width="100%"
            isTabletAndPhone={isTabletAndPhone}
          >
             {children}
          </Content>
        </Block>
      </Desktop>
      <TabletAndPhone>
        <RootTabs
          activeKey={activeKey}
          onChange={a => {
            if (!a.isDisable && !a.isLoading && a.value !== activeKey) {
              handleChange(a)()
            }
            handleChange(a)()
          }}
        >
          {_.map(options, option => (
            <TabPane
              key={option.value}
              tab={
                <Block horizontalPlacement="left" verticalPlacement="center">
                  <Block isMarginRight marginRight={0.5}>{option.label}</Block>
                  <Block>
                    {(() => {
                      if (option.isLoading) {
                        return (
                          <Loader size={1} isColor={false}/>
                        )
                      }

                      if (option.isSuccess && activeKey !== option.value) {
                        return (
                          <Icon
                            iconType="design"
                            icon="check"
                            color={!option.isDisable ? 'success' : 'gray3'}
                            size={1}
                          />
                        )
                      }

                      if (option.isError && activeKey !== option.value) {
                        return (
                          <Icon
                            iconType="design"
                            icon="close"
                            color={!option.isDisable ? 'errorLight' : 'gray3'}
                            size={1}
                          />
                        )
                      }

                      return null
                    })()}
                  </Block>
                </Block>
              }
            >
              <Block isMarginBottom>
                {children}
              </Block>
            </TabPane>
          ))}
        </RootTabs>
      </TabletAndPhone>
    </Fragment>
  )
}

NavBar.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  activeKey: PropTypes.any,
  defaultActiveKey: PropTypes.any,
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.any,
    isSuccess: PropTypes.bool,
    isError: PropTypes.bool,
    isLoading: PropTypes.bool,
    isDisable: PropTypes.bool,
    content: PropTypes.any
  })),
  isFullWidth: PropTypes.bool,
  contentName: PropTypes.string,

  onChange: PropTypes.func
}

NavBar.defaultProps = {
  activeKey: null,
  defaultActiveKey: null,
  isFullWidth: true
}

NavBar.displayName = 'NavBar'

export default withMargin({ displayName: 'NavBar' })(NavBar)
