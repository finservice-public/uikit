import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import DropdownButton from './DropdownButton'

const stories = storiesOf('DropdownButton', module)
const overlay = (
    <div>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, harum.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, harum.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, harum.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, harum.</p>
    </div>
)

stories
  .add('default', () => (
        <DropdownButton
            onClick={action('click')}
            overlay={overlay}
        >
            Dropdown
        </DropdownButton>
  ))
  .add('hover trigger', () => (
    <DropdownButton
        onClick={action('click')}
        overlay={overlay}
        trigger={['hover']}
    >
        Dropdown
    </DropdownButton>
  ))
  .add('visible force', () => (
    <DropdownButton
        onClick={action('click')}
        overlay={overlay}
        isVisible
    >
        Dropdown
    </DropdownButton>
  ))
  .add('placement', () => (
    <DropdownButton
        onClick={action('click')}
        overlay={overlay}
    >
        Dropdown
    </DropdownButton>
  ))
