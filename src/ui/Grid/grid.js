import Row from './Row'
import Col from './Col'

const Grid = {}
Grid.Row = Row
Grid.Col = Col

export { Row, Col }
export default Grid
