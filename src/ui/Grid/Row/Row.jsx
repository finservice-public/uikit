import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import AntdRow from 'antd/es/row'
import styled from 'styled-components'
import withMargin from 'ui/common/withMargin'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isWrap')
  _.unset(resolveProps, 'isRem')
  return <AntdRow {...resolveProps}/>
})`
  flex-wrap: ${props => (props.isWrap ? 'wrap !important' : 'nowrap !important')}
`

const Row = ({
  className, style, htmlID, dataSet,
  isFlex, gutter, justify, align, isWrap,

  onClick, onMouseOver, onMouseLeave,
  children
}) => (
  <Root
    type={isFlex ? 'flex' : null}
    gutter={gutter}
    justify={justify}
    align={align}
    className={className}
    style={style}
    onClick={onClick}
    isWrap={isWrap}
    onMouseOver={onMouseOver}
    onMouseLeave={onMouseLeave}
    {...safeObject({
      id: htmlID || null
    })}
    {...applyHtmlData(dataSet)}
  >
    {children}
  </Root>
)

Row.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  onClick: PropTypes.func,
  onMouseOver: PropTypes.func,
  onMouseLeave: PropTypes.func,

  isFlex: PropTypes.bool,
  gutter: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
  justify: PropTypes.oneOf(['start', 'end', 'center', 'space-around', 'space-between']),
  align: PropTypes.oneOf(['top', 'middle', 'bottom']),
  isWrap: PropTypes.bool
}

Row.defaultProps = {
  isFlex: false,
  gutter: 16,
  justify: 'start',
  align: 'top',
  isWrap: true
}

Row.displayName = 'Row'

export default withMargin({ displayName: 'Row' })(Row)
