import React from 'react'
import PropTypes from 'prop-types'
import AntdPopover from 'antd/es/popover'
import { isSafe, safeObject } from 'utils/object'

const Popover = ({ children, placement, trigger, title, content, isVisible }) => (
  title || content ?
    <AntdPopover
      placement={placement}
      title={title}
      content={content}
      trigger={trigger}
      {...safeObject({
        visible: isSafe(isVisible) ? isVisible : null
      })}
    >
      {children}
    </AntdPopover> : children
)

Popover.propTypes = {
  placement: PropTypes.oneOf([
    'top', 'left', 'right', 'bottom',
    'topLeft', 'topRight',
    'bottomLeft', 'bottomRight',
    'leftTop', 'leftBottom',
    'rightTop', 'rightBottom'
  ]),
  title: PropTypes.any,
  content: PropTypes.any,
  isVisible: PropTypes.bool,
  trigger: PropTypes.oneOf(['hover', 'click', 'focus'])
}

Popover.defaultProps = {
  placement: 'bottom',
  title: null,
  content: null,
  isVisible: null,
  trigger: 'click'
}

Popover.displayName = 'Popover'

export default Popover
