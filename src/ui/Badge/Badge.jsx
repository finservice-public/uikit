import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import withMargin from 'ui/common/withMargin'
import AntBadge from 'antd/es/badge'
import styled from 'styled-components'
import { colors } from 'styles'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'background')
  _.unset(resolveProps, 'color')
  return <span {...resolveProps}/>
})`
  padding: 2px 6px;
  border-radius: 10px;
`

const Badge = ({
  className, style, htmlID, dataSet,

  label, color, background, status, statusText, size
}) => (
  statusText === '' ?
    <Root
      className={className}
      style={safeObject({
        background: background ? colors[background] ? colors[background] : background :
          colors.white,
        boxShadow: !background ? '0 0 0 1px #d9d9d9 inset' : null,
        color: color ? colors[color] ? colors[color] : color : background ? 'white' :
          colors.text,
        fontSize: `${size * 14}px`,
        ...style
      })}
      {...safeObject({ id: htmlID ?? null })}
      {...applyHtmlData(dataSet)}
    >
      {label}
    </Root> :
    <AntBadge
      className={className}
      style={style}
      status={status}
      text={statusText}
    />
)

Badge.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  label: PropTypes.any,
  color: PropTypes.string, // colors of theme or custom color,
  background: PropTypes.string, // colors of theme or custom color
  status: PropTypes.oneOf(['success', 'processing', 'default', 'error', 'warning']),
  statusText: PropTypes.string,
  size: PropTypes.number
}

Badge.defaultProps = {
  label: 0,
  color: 'white',
  background: 'primary',
  status: 'default',
  statusText: '',
  size: 1
}

Badge.displayName = 'Badge'

export default withMargin({ displayName: 'Badge' })(Badge)
