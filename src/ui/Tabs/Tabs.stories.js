import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Tabs from './Tabs'

const stories = storiesOf('Tabs', module)

stories
  .add('default', () => (
        <Tabs
            options={[
              { value: 'one', label: 'One', node: 'One' },
              { value: 'two', label: 'Two', node: 'Two' },
              { value: 'three', label: 'Three', node: 'Three' },
              { value: 'four', label: 'Four', node: 'Four' }
            ]}
        />
  ))
