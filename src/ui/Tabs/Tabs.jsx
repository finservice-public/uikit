import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import AntTabs from 'antd/es/tabs'
import { isSafe, safeObject } from 'utils/object'
import withMargin from 'ui/common/withMargin'
import { applyHtmlData } from 'utils/htmlData'

const { TabPane } = AntTabs

const Tabs = ({
  className, style, htmlID, dataSet,
  active, options, onChange, tabBarExtraContent
}) => {
  const handleChange = v => {
    if (onChange) onChange(v)
  }

  return (
        <AntTabs
            className={className}
            style={style}
            {...safeObject({
              activeKey: isSafe(active) && active !== '' ? active : null,
              tabBarExtraContent,
              id: htmlID || null
            })}
            {...applyHtmlData(dataSet)}
            onChange={handleChange}
        >
            {_.map(options, option => (
                <TabPane
                    key={option.value}
                    tab={option.label}
                    {...safeObject({ id: htmlID ? `${htmlID}-${option.value}` : null })}
                >
                    {option.node}
                </TabPane>
            ))}
        </AntTabs>
  )
}

Tabs.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  active: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.any,
    node: PropTypes.any
  })).isRequired,
  onChange: PropTypes.func,
  tabBarExtraContent: PropTypes.any
}

Tabs.defaultProps = {
  tabBarExtraContent: null
}

Tabs.displayName = 'Tabs'

export default withMargin({ displayName: 'Tabs' })(Tabs)
