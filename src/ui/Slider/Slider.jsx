import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'ui/Grid'
import { Text } from 'ui/Typography'
import AntSlider from 'antd/es/slider'
import withMargin from 'ui/common/withMargin'
import styled from 'styled-components'
import Block from 'ui/Block'
import _ from 'lodash'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Root = styled(Block)`
  user-select: none;
  
  & * {
    user-select: none;
  }
`

const Slider = ({
  className, style, htmlID, dataSet,
  value, min, max, step, isDisable, isFullWidth, tabIndex,
  onChange, sliderNormalizer
}) => {
  const [currentValue, setValue] = useState(0)

  const handleChange = v => {
    setValue(v)
    if (onChange) onChange(v)
  }

  useEffect(() => {
    if (value >= min && value <= max) {
      setValue(value)
    }
  }, [value])

  const width = _.get(style, 'width', null)

  return (
    <Root
      className={className}
      style={safeObject({
        ...style,
        width: isFullWidth ? '100%' : width
      })}
    >
      <AntSlider
        value={currentValue >= min ? currentValue : min}
        min={Number(min) || 0}
        max={Number(max) > Number(min) ? Number(max) : 1}
        step={Number(max) > step ? step : 1}
        disabled={isDisable}
        onChange={handleChange}
        {...safeObject({
          tipFormatter: sliderNormalizer || null,
          tabIndex,
          id: htmlID || null
        })}
        {...applyHtmlData(dataSet)}
      />
      <Row isFlex justify="space-between" isMarginTop marginTop={-0.5}>
        <Col><Text type="secondary">{sliderNormalizer ? sliderNormalizer(min) : min}</Text></Col>
        <Col><Text type="secondary">{sliderNormalizer ? sliderNormalizer(max) : max}</Text></Col>
      </Row>
    </Root>
  )
}


Slider.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.any,

  value: PropTypes.number,
  min: PropTypes.number,
  max: PropTypes.number,
  step: PropTypes.number,
  isDisable: PropTypes.bool,
  isFullWidth: PropTypes.bool,
  tabIndex: PropTypes.any,

  onChange: PropTypes.func,
  sliderNormalizer: PropTypes.func
}

Slider.defaultProps = {
  value: 0,
  min: 0,
  max: 100,
  step: 1,
  isDisable: false,
  isFullWidth: true,
  tabIndex: null
}

Slider.displayName = 'Slider'

export default withMargin({ displayName: 'Slider' })(Slider)
