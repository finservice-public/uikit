import _ from 'lodash'
import fp from 'lodash/fp'
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import withMargin from 'ui/common/withMargin'
import withFormProps from 'ui/common/withFormProps'
import Radio from 'antd/es/radio'
import styled from 'styled-components'
import { applyHtmlData } from 'utils/htmlData'
import { isSafe, safeObject } from 'utils/object'

const Root = styled(Radio.Group)`
  user-select: none;
  width: 100%;
  
  & * {
    user-select: none;
  }
`

const CheckBlock = ({
  style, className, htmlID, dataSet,

  value, defaultValue, options, isFill, size, isFullWidth, tabIndex, isDisable,

  onChangeIsInput, onChangeIsFocus, onChange
}) => {
  const [currentValue, setValue] = useState(null)

  const handleChange = e => {
    const v = _.get(e, 'target.value', null)
    if (v) {
      if (onChangeIsInput) onChangeIsInput(true)
      setValue(v)
      if (onChange) onChange(v)
    }
  }

  useEffect(() => {
    setValue(value)
  }, [value])

  const width = _.get(style, 'width', null)

  return (
    <Root
      className={className}
      onChange={handleChange}
      onFocus={onChangeIsFocus}
      disabled={isDisable}
      {...safeObject({
        buttonStyle: isFill ? 'solid' : null,
        size: size === 'lg' ? 'large' : size === 'xs' ? 'small' : null,
        width: isFullWidth ? '100%' : width,
        ...style,
        tabIndex,
        id: htmlID || null
      })}
      {...safeObject({
        value: currentValue,
        defaultValue: !isSafe(value) ? defaultValue : null
      })}
      {...applyHtmlData(dataSet)}
    >
      {fp.map(option => (
        <Radio.Button
          key={option.value}
          value={option.value}
          {...safeObject({
            tabIndex,
            id: htmlID ? `${htmlID}-${option.value}` : null
          })}
        >
          {option.label}
        </Radio.Button>
      ))(options)}
    </Root>
  )
}

CheckBlock.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  value: PropTypes.any,
  defaultValue: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.any,
    label: PropTypes.any
  })),
  isFill: PropTypes.bool,
  size: PropTypes.oneOf(['xs', 'sm', 'lg']),
  isFullWidth: PropTypes.bool,
  tabIndex: PropTypes.any,
  isDisable: PropTypes.bool,

  onChange: PropTypes.func,

  // withFormProps
  onChangeIsInput: PropTypes.func,
  onChangeIsFocus: PropTypes.func
}

CheckBlock.defaultProps = {
  value: null,
  defaultValue: null,
  isFill: false,
  size: 'lg',
  isFullWidth: true,
  tabIndex: null,
  isDisable: false
}

CheckBlock.displayName = 'CheckBlock'

export default withMargin({
  displayName: 'CheckBlock'
})(withFormProps({
  displayName: 'CheckBlock'
})(CheckBlock))
