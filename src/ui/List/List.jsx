import _ from 'lodash'
import React, { Fragment, useState } from 'react'
import PropTypes from 'prop-types'
import AntList from 'antd/es/list'
import withMargin from 'ui/common/withMargin'
import Button from 'ui/Button'
import { Row, Col } from 'ui/Grid'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'
import Item from './ListItem'

const getArrayFromPage = (data, page, offset) => _.slice(data, (page - 1) * offset, (page - 1) * offset + offset)

const List = ({
  className, style, htmlID, dataSet,

  data, pageSize, renderItem, header, footer, isPagination, paginationPlacement,
  isMore, moreLabel, moreLabelExpand, moreCount, moreLabelPlacement,
  isLoading
}) => {
  const [page, setPage] = useState(1)
  const [isExpand, setIsExpand] = useState(false)

  const handleChangePage = p => {
    setPage(p)
  }

  const handleExpand = () => setIsExpand(!isExpand)

  const listProps = {
    itemLayout: 'vertical',
    dataSource: isPagination ? getArrayFromPage(data, page, pageSize) : !isMore ? data :
      !isExpand ? [...data].splice(0, moreCount) : data,
    large: 'large',
    renderItem,
    ...safeObject({
      id: htmlID || null,
      ...applyHtmlData(dataSet),
      header,
      footer,
      pagination: isPagination && !isMore ? {
        pageSize,
        total: data.length,
        current: page,
        onChange: handleChangePage,
        position: paginationPlacement
      } : null,
      loadMore: isMore && data.length > moreCount ? (
          <Row
              isMarginTop
              isFlex
              justify={(() => {
                switch (moreLabelPlacement) {
                  case 'left':
                    return 'start'
                  case 'right':
                    return 'end'
                  case 'center':
                    return 'center'
                  default:
                    return 'center'
                }
              })()}
          >
            <Col>
              <Button
                  icon={!isExpand ? 'down' : 'up'}
                  size="sm"
                  onClick={handleExpand}
                  {...safeObject({ htmlID: htmlID ? `${htmlID}-expand` : null })}
              >
                {!isExpand ? moreLabel : moreLabelExpand}
              </Button>
            </Col>
          </Row>
      ) : null
    })
  }

  return (
    <Fragment>
      <AntList
        className={className}
        loading={isLoading}
        style={style}
        {...listProps}
      />
    </Fragment>
  )
}

List.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.any,

  data: PropTypes.array,
  renderItem: PropTypes.func.isRequired,
  pageSize: PropTypes.number,
  header: PropTypes.any,
  footer: PropTypes.any,
  isPagination: PropTypes.bool,
  paginationPlacement: PropTypes.oneOf(['bottom', 'top', 'both']),
  isMore: PropTypes.bool,
  moreLabel: PropTypes.string,
  moreLabelExpand: PropTypes.string,
  moreCount: PropTypes.number,
  moreLabelPlacement: PropTypes.oneOf(['left', 'center', 'right']),
  isLoading: PropTypes.bool
}

List.defaultProps = {
  data: [],
  pageSize: 10,
  header: null,
  footer: null,
  isPagination: false,
  paginationPlacement: 'bottom',
  isMore: false,
  moreLabel: 'Раскрыть',
  moreLabelExpand: 'Скрыть',
  moreCount: 5,
  moreLabelPlacement: 'center',
  isLoading: false
}

const ListItem = Item

List.displayName = 'List'
ListItem.displayName = 'ListItem'

export { ListItem }

export default withMargin({ displayName: 'List' })(List)
