import _ from 'lodash'
import React from 'react'
import { storiesOf } from '@storybook/react'
import { Paragraph, Title } from 'ui/Typography'
import List, { ListItem } from 'ui/List'
import exampleExtra from './exampleExtra.jpg'

const stories = storiesOf('List', module)

stories.addDecorator(story => (
    <div style={{ width: '50%' }}>
        {story()}
    </div>
))

const label = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ad, amet aperiam dolor doloremque esse explicabo ipsam labore laboriosam maiores neque nesciunt officiis, quos reiciendis reprehenderit sed similique ullam voluptate.'

const data = _.map(_.times(20), t => ({
  title: 'Any title',
  description: 'Any description',
  extra: (
    <img
        width={272}
        alt="logo"
        src={exampleExtra}
    />
  ),
  label: `${t + 1}. ${label}`
}))

const renderSimpleItem = item => (
  <Paragraph>{item.label}</Paragraph>
)

stories
  .add('default', () => (
    <List renderItem={renderSimpleItem} data={data}/>
  ))
  .add('with header', () => (
    <List
        renderItem={renderSimpleItem}
        data={data}
        header={<Title>List</Title>}
    />
  ))
  .add('with header & footer', () => (
    <List
        renderItem={renderSimpleItem}
        data={data}
        header={<Title>List</Title>}
        footer={<Paragraph isStrong>Footer</Paragraph>}
    />
  ))
  .add('pagination', () => (
    <List
        renderItem={renderSimpleItem}
        data={data}
        header={<Title>List</Title>}
        footer={<Paragraph isStrong>Footer</Paragraph>}
        isPagination
    />
  ))
  .add('pagination & pageSize = 6', () => (
    <List
        renderItem={renderSimpleItem}
        data={data}
        header={<Title>List</Title>}
        footer={<Paragraph isStrong>Footer</Paragraph>}
        isPagination
        pageSize={6}
    />
  ))
  .add('pagination top', () => (
    <List
        renderItem={renderSimpleItem}
        data={data}
        header={<Title>List</Title>}
        footer={<Paragraph isStrong>Footer</Paragraph>}
        pageSize={6}
        isPagination
        paginationPlacement="top"
    />
  ))
  .add('pagination bottom & top', () => (
    <List
        renderItem={renderSimpleItem}
        data={data}
        header={<Title>List</Title>}
        footer={<Paragraph isStrong>Footer</Paragraph>}
        pageSize={6}
        isPagination
        paginationPlacement="both"
    />
  ))
  .add('loading', () => (
    <List
        renderItem={renderSimpleItem}
        data={data}
        header={<Title>List</Title>}
        footer={<Paragraph isStrong>Footer</Paragraph>}
        isLoading
    />
  ))
  .add('loading', () => (
    <List
        renderItem={renderSimpleItem}
        data={data}
        header={<Title>List</Title>}
        footer={<Paragraph isStrong>Footer</Paragraph>}
        isLoading
    />
  ))
  .add('with title', () => (
    <List
        renderItem={item => (
          <ListItem title={item.title}>
            <Paragraph>{item.label}</Paragraph>
          </ListItem>
        )}
        data={data}
        header={<Title>List</Title>}
        footer={<Paragraph isStrong>Footer</Paragraph>}
    />
  ))
  .add('with title & description', () => (
    <List
        renderItem={item => (
          <ListItem
              title={item.title}
              description={item.description}
          >
            <Paragraph>{item.label}</Paragraph>
          </ListItem>
        )}
        data={data}
        header={<Title>List</Title>}
        footer={<Paragraph isStrong>Footer</Paragraph>}
    />
  ))
  .add('with title & description', () => (
    <List
        renderItem={item => (
            <ListItem
                title={item.title}
                description={item.description}
            >
              <Paragraph>{item.label}</Paragraph>
            </ListItem>
        )}
        data={data}
        header={<Title>List</Title>}
        footer={<Paragraph isStrong>Footer</Paragraph>}
    />
  ))
  .add('with title & description & extra', () => (
    <List
        renderItem={item => (
            <ListItem
                title={item.title}
                description={item.description}
                extra={item.extra}
            >
                <Paragraph>{item.label}</Paragraph>
            </ListItem>
        )}
        data={data}
        header={<Title>List</Title>}
        footer={<Paragraph isStrong>Footer</Paragraph>}
    />
  ))
  .add('more', () => (
    <List
        renderItem={item => (
            <ListItem
                title={item.title}
                description={item.description}
                extra={item.extra}
            >
                <Paragraph>{item.label}</Paragraph>
            </ListItem>
        )}
        data={data}
        header={<Title>List</Title>}
        isMore
    />
  ))
