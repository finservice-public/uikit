import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import storiesHelpers from 'ui/common/withFormProps/storiesHelpers'
import Checkbox, { CheckboxGroup } from './Checkbox'

const stories = storiesOf('Checkbox/CheckboxGroup', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

stories
  .add('group', () => (
    <CheckboxGroup onChange={action('group changed')}>
      <Checkbox value="one">One</Checkbox>
      <Checkbox value="twp">Two</Checkbox>
      <Checkbox value="three">Three</Checkbox>
    </CheckboxGroup>
  ))


storiesHelpers({
  stories,
  Component: props => (
    <CheckboxGroup onChange={action('group changed')} {...props}>
        <Checkbox value="one">One</Checkbox>
        <Checkbox value="twp">Two</Checkbox>
        <Checkbox value="three">Three</Checkbox>
    </CheckboxGroup>
  ),
  isOnlyForceValidation: true,
  isWidth: false
})

stories
  .add('changed', () => (
    <Checkbox onChange={action('changed')}>Changed</Checkbox>
  ))
