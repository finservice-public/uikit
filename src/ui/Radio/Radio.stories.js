import React from 'react'
import { storiesOf } from '@storybook/react'
import storiesHelpers from 'ui/common/withFormProps/storiesHelpers'
import Radio from './Radio'

const stories = storiesOf('Radio/Radio', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

stories
  .add('default', () => (
      <>
        <Radio>Radio</Radio>
        <Radio>Radio</Radio>
      </>
  ))

storiesHelpers({
  stories,
  Component: props => <Radio {...props}>Radio</Radio>,
  isOnlyForceValidation: true,
  isWidth: false
})
