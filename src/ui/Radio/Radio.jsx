import _ from 'lodash'
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import AntdRadio from 'antd/es/radio'
import withFormProps from 'ui/common/withFormProps'
import styled from 'styled-components'
import withMargin from 'ui/common/withMargin'
import { applyHtmlData } from 'utils/htmlData'
import { isSafe, safeObject } from 'utils/object'
import { colors } from 'styles'
import Group from './Group'

const Root = styled(AntdRadio)`
  user-select: none;
  transition: color .3s ease-in-out;
  
  &:hover {
    color: ${colors.primary6};
  }
  
  * {
    user-select: none;
  }
`

const Radio = ({
  className, style, htmlID, dataSet,

  isDisable, value, tabIndex,
  children,
  ...props
}) => {
  const [isChecked, setIsChecked] = useState(false)

  const handleChange = () => {
    setIsChecked(!isChecked)
    if (props.onChange) props.onChange(!isChecked)
  }

  useEffect(() => {
    if (isSafe(_.get(props, 'isChecked', null))) setIsChecked(props.isChecked)
  }, [props.isChecked])

  return (
    <Root
      className={className}
      style={style}
      checked={isChecked}
      value={value}
      disabled={isDisable}
      onChange={handleChange}
      {...safeObject({
        tabIndex,
        id: htmlID || null
      })}
      {...applyHtmlData(dataSet)}
    >
      {children}
    </Root>
  )
}

Radio.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.any,

  value: PropTypes.any,
  isChecked: PropTypes.bool,
  isDisable: PropTypes.bool,
  tabIndex: PropTypes.number,

  onChange: PropTypes.func
}

Radio.defaultProps = {
  isChecked: false,
  isDisable: false,
  tabIndex: null
}

Radio.displayName = 'Radio'

export const RadioGroup = withMargin({ displayName: 'RadioGroup', forceIsRem: false })(withFormProps({
  isOnlyShowForceValidate: true,
  displayName: 'RadioGroup'
})(Group))

export default withMargin()(withFormProps({
  isOnlyShowForceValidate: true,
  displayName: 'Radio'
})(Radio))
