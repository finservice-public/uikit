import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import storiesHelpers from 'ui/common/withFormProps/storiesHelpers'
import Radio, { RadioGroup } from './Radio'

const stories = storiesOf('Radio/RadioGroup', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

stories
  .add('group', () => (
      <RadioGroup onChange={action('group changed')}>
        <Radio value="one">One</Radio>
        <Radio value="two">Two</Radio>
        <Radio value="three">Three</Radio>
      </RadioGroup>
  ))
  .add('changed', () => (
    <Radio onChange={action('changed')}>Changed</Radio>
  ))

storiesHelpers({
  stories,
  Component: props => (
    <RadioGroup onChange={action('group changed')} {...props}>
        <Radio value="one">One</Radio>
        <Radio value="two">Two</Radio>
        <Radio value="three">Three</Radio>
    </RadioGroup>
  ),
  isOnlyForceValidation: true,
  isWidth: false
})
