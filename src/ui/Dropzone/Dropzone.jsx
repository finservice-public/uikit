import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import { useDropzone } from 'react-dropzone'
import { safeObject } from 'utils/object'
import styled from 'styled-components'

const Root = styled.div`
  outline: none;
`

const Dropzone = ({
  onDrop,
  onDropAccepted,
  onDropRejected,
  accept,
  multiple,
  htmlID,
  minSize,
  maxSize,
  isDisable,
  renderComponent
}) => {
  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject
  } = useDropzone({
    ...safeObject({
      onDrop: onDrop ? useCallback((acceptedFiles, rejectedFiles) => onDrop(acceptedFiles, rejectedFiles)) : null,
      onDropAccepted: onDropAccepted ? useCallback((acceptedFiles) => onDropAccepted(acceptedFiles)) : null,
      onDropRejected: onDropRejected ? useCallback((rejectedFiles) => onDropRejected(rejectedFiles)) : null,
      accept,
      multiple,
      minSize,
      maxSize,
      disabled: isDisable
    })
  })

  return (
    <Root {...getRootProps()} id={htmlID}>
      <input {...getInputProps()} />
      {renderComponent({ isDragActive, isDragAccept, isDragReject, isDisable })}
    </Root>
  )
}

Dropzone.propTypes = {
  onDrop: PropTypes.func,
  onDropAccepted: PropTypes.func,
  onDropRejected: PropTypes.func,
  accept: PropTypes.string,
  multiple: PropTypes.bool,
  htmlID: PropTypes.string,
  minSize: PropTypes.number,
  maxSize: PropTypes.number,
  isDisable: PropTypes.bool,
  renderComponent: PropTypes.func.isRequired
}

Dropzone.defaultProps = {
  multiple: false,
  isDisable: false
}

export default Dropzone
