import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import Steps from 'antd/es/steps'
import Icon from 'ui/Icon'
import withMargin from 'ui/common/withMargin'
import styled from 'styled-components'
import Loader from 'ui/Loader'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const { Step } = Steps

const Root = styled(Steps)`
  user-select: none;
  
  & * {
    user-select: none;
  }

  & .ant-steps-item-description {
    max-width: 100% !important;
  }
`

const Stepper = ({
  className, style, htmlID, dataSet,

  current, options, placement, onChange
}) => (
    <Root
        className={className}
        style={style}
        current={current}
        direction={placement}
        {...safeObject({
          onChange: onChange || null,
          id: htmlID || null
        })}
        {...applyHtmlData(dataSet)}
    >
        {_.map(options, (option, index) => (
            <Step
                key={index}
                {...safeObject({
                  title: option.title || null,
                  description: option.description || null,
                  icon: option.status !== 'process' ? option.icon ? <Icon icon="icon"/> : null : <Loader isColor={false}/>,
                  status: option.status || null,
                  id: htmlID ? `${htmlID}-${option.id}` : null
                })}
            />
        ))}
    </Root>
)

Stepper.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.any,

  current: PropTypes.number,
  placement: PropTypes.oneOf(['horizontal', 'vertical']),
  options: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.any,
    description: PropTypes.any,
    icon: PropTypes.string,
    status: PropTypes.oneOf(['success', 'wait', 'process', 'finish', 'error'])
  })),

  onChange: PropTypes.func
}

Stepper.defaultProps = {
  current: 0,
  options: [],
  placement: 'horizontal'
}

Stepper.displayName = 'Stepper'

export default withMargin({ displayName: 'Stepper' })(Stepper)
