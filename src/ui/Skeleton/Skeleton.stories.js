import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import Block from 'ui/Block'
import Skeleton from './Skeleton'

const stories = storiesOf('Skeleton', module)

stories.addDecorator(story => (
  <div style={{ width: '40rem' }}>
    {story()}
  </div>
))

stories
  .add('default', () => (
    <Skeleton />
  ))
  .add('animated', () => (
    <Fragment>
      <Skeleton isAnimate width="30%" isMarginBottom/>
      <Skeleton isAnimate width="70%" isMarginBottom/>
      <Skeleton isAnimate width="100%"/>
    </Fragment>
  ))
  .add('circle', () => (
    <Skeleton isAnimate width={2} isCircle/>
  ))
  .add('complex', () => (
    <Fragment>
      <Block horizontalPlacement="left" verticalPlacement="center" isMarginBottom>
        <Skeleton width={2} isCircle isMarginRight/>
        <Skeleton width={10}/>
      </Block>
      <Block horizontalPlacement="left" verticalPlacement="center" isMarginBottom>
        <Skeleton width={2} isCircle isMarginRight/>
        <Skeleton width={15}/>
      </Block>
      <Block horizontalPlacement="left" verticalPlacement="center" isMarginBottom marginBottom={2}>
        <Skeleton width={2} isCircle isMarginRight/>
        <Skeleton width={20}/>
      </Block>

      <Block horizontalPlacement="left" verticalPlacement="center" isMarginBottom>
        <Skeleton width={2} isCircle isAnimate isMarginRight/>
        <Skeleton width={10} isAnimate/>
      </Block>
      <Block horizontalPlacement="left" verticalPlacement="center" isMarginBottom>
        <Skeleton width={2} isCircle isAnimate isMarginRight/>
        <Skeleton width={15} isAnimate/>
      </Block>
      <Block horizontalPlacement="left" verticalPlacement="center">
        <Skeleton width={2} isCircle isAnimate isMarginRight/>
        <Skeleton width={20} isAnimate/>
      </Block>
    </Fragment>
  ))
