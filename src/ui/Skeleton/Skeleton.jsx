import _ from 'lodash'
import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import withMargin from 'ui/common/withMargin'
import styled, { keyframes, css } from 'styled-components'
import { lighten } from 'utils/colors'
import { colors } from 'styles'
import { Context } from 'ui/Provider/Context'

const animationFunc = keyframes`
  0% {
    background-position: 100% 50%;
  }
  100% {
    background-position: 0 50%;
  }
`

const animation = css`
  animation: ${animationFunc} 1.4s ease infinite;
`

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isAnimate')
  _.unset(resolveProps, 'isCircle')
  _.unset(resolveProps, 'isTouch')
  _.unset(resolveProps, 'color')
  return <div {...resolveProps}/>
})`
  ${props => (props.isAnimate ? animation : 'animation: none;')}
  background: ${props => `linear-gradient(90deg, ${lighten(props.color, 0.07)} 25%, ${props.color} 37%, ${lighten(props.color, 0.07)} 63%)`};
  background-size: 400% 100%;
  border-radius: ${props => (props.isCircle ? '50%' : !props.isTouch ? '4px' : '8px')};
`

const Skeleton = ({
  className, style,

  color, isAnimate, isCircle
}) => {
  const { responsiveModel } = useContext(Context)

  const width = _.get(style, 'width', '140px')
  const height = _.get(style, 'height', '28px')

  return (
    <Root
      className={className}
      isAnimate={isAnimate}
      isCircle={isCircle}
      isTouch={responsiveModel.isTabletAndPhone}
      color={colors[color] ? colors[color] : color}
      style={{
        ...style,
        width,
        height: !isCircle ? height : width
      }}
    />
  )
}

Skeleton.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,

  isAnimate: PropTypes.bool,
  isCircle: PropTypes.bool,
  color: PropTypes.string // colors of theme or custom color
}

Skeleton.defaultProps = {
  isAnimate: false,
  isCircle: false,
  color: 'gray3'
}

Skeleton.displayName = 'Skeleton'

export default withMargin()(Skeleton)
