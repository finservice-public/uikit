import React from 'react'
import Block from 'ui/Block'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Breadcrumb from './Breadcrumb'

const stories = storiesOf('Breadcrumb', module)

const options = [
  { node: 'One' },
  { node: 'Two' },
  { node: <Block>Three</Block> }
]

stories
  .add('default', () => (
    <Breadcrumb options={options}/>
  ))
  .add('with underline', () => (
    <Breadcrumb
        options={[
          { node: 'Active item', isUnderline: true },
          { node: 'Two' },
          { node: <Block>Three</Block> }
        ]}
    />
  ))
  .add('with click', () => (
      <Breadcrumb
          options={[
            { node: 'Active item', isUnderline: true },
            { node: 'Two' },
            { node: <Block>Three</Block>, onClick: action('clicked') }
          ]}
      />
  ))
