import _ from 'lodash'
import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import withMargin from 'ui/common/withMargin'
import styled from 'styled-components'
import { Context } from 'ui/Provider/Context'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isClick')
  _.unset(resolveProps, 'isBlock')
  return <img {...resolveProps}/>
})`
  cursor: ${props => (props.isClick ? 'pointer' : 'inherit')};
  display: ${props => (props.isBlock ? 'block' : 'inline-block')};
  
  user-select: none;
`

const Image = ({
  className, style, htmlID, dataSet,

  src, isAvatar, isBlock, isRem, borderRadius,
  onClick, ...props
}) => {
  const width = _.get(props, 'width', null)
  const height = _.get(props, 'height', null)
  const { remPoint } = useContext(Context)
  const point = isRem ? remPoint : 1

  return (
    src ?
      <Root
        className={className}
        isClick={!!onClick}
        isBlock={isBlock}
        src={src}
        style={safeObject({
          ...style,
          maxWidth: width ? _.isNumber(width) ? `${width * point}px` : width : '100%',
          maxHeight: height ? _.isNumber(height) ? `${height * point}px` : height : '100%',
          borderRadius: isAvatar ? '50%' : borderRadius ?
            _.isNumber(borderRadius) ? `${borderRadius * point}px` : borderRadius : null
        })}
        onClick={onClick}
        {...safeObject({
          id: htmlID || null
        })}
        {...applyHtmlData(dataSet)}
      /> : null
  )
}

Image.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  isRem: PropTypes.bool,
  src: PropTypes.string,
  isAvatar: PropTypes.bool,
  isBlock: PropTypes.bool,
  borderRadius: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  onClick: PropTypes.func
}

Image.defaultProps = {
  src: null,
  isRem: true,
  isAvatar: false,
  isBlock: false
}

Image.displayName = 'Image'

export default withMargin({ isWidth: false, isHeight: false, displayName: 'Image' })(Image)
