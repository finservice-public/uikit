import _ from 'lodash'
import React, { Fragment, useContext } from 'react'
import PropTypes from 'prop-types'
import AntModal from 'antd/es/modal'
import Block from 'ui/Block'
import Icon from 'ui/Icon'
import styled from 'styled-components'
import { colors } from 'styles'
import { Title } from 'ui/Typography'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'
import { Context } from '../Provider/Context'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isMobile')
  return <AntModal {...resolveProps}/>
})`
  position: relative;
  transform-origin: ${props => (props.isMobile ? 'top !important' : 'top')};
  width: ${props => (props.isMobile ? '100% !important' : 'auto')};
  height: auto;
  max-width: ${props => (props.isMobile ? '100% !important' : 'auto')};
  margin: ${props => (props.isMobile ? '0 !important' : 'auto')};
  top: ${props => (props.isMobile ? '0 !important' : -1)};
  min-height: ${props => (props.isMobile ? '100% !important' : '10%')};
  padding-bottom: ${props => (props.isMobile ? '0 !important' : '24px')};
  padding: ${props => (props.isMobile ? '16px !important' : '-1')};
  overflow-x: hidden;
  
  & .ant-modal-header {
    padding: 0 !important;
  }
  
  & .ant-modal-content {
    min-height: ${props => (props.isMobile ? '100% !important' : '10%')};
    margin-bottom: ${props => (props.isMobile ? '24px !important' : 0)};
  }
`

const CloseIcon = styled(Block)`
  position: absolute;
  top: 14px;
  right: 14px;
`

const Modal = ({
  className, style, htmlID, dataSet,

  title, isOpen, isCenter, isClosable,
  footer, bodyStyle, width, height,
  okText, okType, cancelText,
  onOk, onCancel, onAfterClose,
  children
}) => {
  const { responsiveModel: { isMobile } } = useContext(Context)
  const contentStyle = {
    ...bodyStyle
  }
  if (isMobile) {
    _.unset(contentStyle, 'width')
    _.unset(contentStyle, 'height')
  }

  const close = (
      <Block
          isCenter
          isRem={false}
          width={24}
          height={24}
          background={colors.red1}
          onClick={onCancel}
          borderRadius={8}
          htmlID={htmlID}
      >
        <Icon icon="close" iconType="design" color="red3" size={16} isRem={false}/>
      </Block>
  )

  return (
      <Fragment>
        <Root
            className={className}
            style={style}
            title={
              <Block
                  flex={1}
                  horizontalPlacement="between"
                  padding={!isMobile ? '16px 40px' : '16px'}
              >
                {_.isString(title) ?
                    <Block isCenter width="100%">
                      <Title level={4}>{title}</Title>
                    </Block> : title
                }
                {isClosable && <CloseIcon>{close}</CloseIcon>}
              </Block>
            }
            isMobile={isMobile}
            visible={isOpen}
            closable={false}
            centered={isCenter}
            bodyStyle={safeObject({
              height: height || null,
              ...contentStyle,
              padding: !isMobile ? '0 40px 16px 40px' : '0 16px 16px 16px'
            })}
            footer={footer}
            okText={okText}
            okType={okType}
            cancelText={cancelText}
            width={width}
            onOk={onOk}
            onCancel={onCancel}
            afterClose={onAfterClose}
            {...safeObject({
              id: htmlID || null
            })}
            {...applyHtmlData(dataSet)}
        >
          {children}
        </Root>
      </Fragment>
  )
}

Modal.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  title: PropTypes.any,
  isOpen: PropTypes.bool,
  isCenter: PropTypes.bool,
  isClosable: PropTypes.bool,
  footer: PropTypes.any,
  bodyStyle: PropTypes.object,
  okText: PropTypes.string,
  okType: PropTypes.oneOf(['default', 'primary', 'danger', 'ghost']),
  cancelText: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  onOk: PropTypes.func,
  onCancel: PropTypes.func,
  onAfterClose: PropTypes.func
}

Modal.defaultProps = {
  isOpen: false,
  isCenter: false,
  isClosable: true,
  footer: null,
  okText: 'Ок',
  okType: 'primary',
  cancelText: 'Отмена',
  width: 520
}

Modal.displayName = 'Modal'

export default Modal
