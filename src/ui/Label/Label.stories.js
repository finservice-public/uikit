import React from 'react'
import { storiesOf } from '@storybook/react'
import Input from 'ui/Input'
import CheckBlock from 'ui/CheckBlock'
import { Row, Col } from 'ui/Grid'
import Label from './Label'

const stories = storiesOf('Label', module)

stories.addDecorator(story => (
  <div style={{ width: '40rem' }}>
    {story()}
  </div>
))

const Content = () => (
  <Row>
    <Col md={6}>
      <Input error="Ошибка"/>
    </Col>
    <Col md={6}>
      <CheckBlock
        options={[
          { value: 'one', label: '₽' },
          { value: 'two', label: 'Процент, %' },
          { value: 'three', label: 'мес.' }
        ]}
      />
    </Col>
  </Row>
)

stories
  .add('default', () => (
    <Label label="Label">
      <Content/>
    </Label>
  ))
  .add('horizontal', () => (
    <Label label="Label" isVerticalLabel={false} labelCol={1}>
      <Content/>
    </Label>
  ))
