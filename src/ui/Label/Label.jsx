import _ from 'lodash'
import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import Form from 'antd/es/form'
import withMargin from 'ui/common/withMargin'
import styled from 'styled-components'
import { Context } from 'ui/Provider/Context'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'weight')
  return <Form.Item {...resolveProps}/>
})`
  font-weight: ${props => props.weight};

  & .ant-form-item-label {
    font-size: 14px !important;
    padding-bottom: 0 !important;
  }

  & .ant-form-item {
    margin-bottom: 0;
  } 
  
  & label {
    margin-bottom: 0;
  }
  
  & * {
    font-weight: ${props => props.weight};
  }
  
  & .ant-form-item-label {
    text-align: left;
    line-height: 1.7 !important;
  }
`

const Label = ({
  className, style, htmlID, dataSet,
  label, isVerticalLabel, labelCol,
  isFullWidth,
  children
}) => {
  const width = _.get(style, 'width', null)
  const weight = _.get(useContext(Context), 'label.weight', 600)

  return (
    <Root
      className={className}
      weight={weight}
      style={safeObject({
        width: isFullWidth && !width ? '100%' : width,
        ...style
      })}
      label={label || null}
      colon={false}
      {...!isVerticalLabel ? {
        wrapperCol: {
          xs: 12,
          md: 12 - labelCol
        },
        labelCol: {
          xs: 12,
          md: labelCol
        }
      } : null}
      {...safeObject({
        id: htmlID || null
      })}
      {...applyHtmlData(dataSet)}
    >
      {children}
    </Root>
  )
}


Label.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  label: PropTypes.any,
  isVerticalLabel: PropTypes.bool,
  labelCol: PropTypes.number,
  isFullWidth: PropTypes.bool
}

Label.defaultProps = {
  label: null,
  isFullWidth: true,
  isVerticalLabel: true,
  labelCol: 2
}

Label.displayName = 'Label'

export default withMargin({ displayName: 'Label' })(Label)
