import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import { Divider } from 'ui/Typography'
import Progress from './Progress'

const stories = storiesOf('Progress', module)

stories.addDecorator(story => (
    <div style={{ width: '70%' }}>
      {story()}
    </div>
))

stories
  .add('default', () => (
    <Fragment>
      <Progress/>
      <Divider/>
      <Progress type="circle"/>
      <Divider/>
      <Progress type="dashboard"/>
    </Fragment>
  ))
  .add('with percent', () => (
    <Fragment>
      <Fragment>
        <Progress percent={20} isMarginBottom/>
        <Progress percent={50} isMarginBottom/>
        <Progress percent={70} isMarginBottom/>
        <Progress percent={100}/>
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="circle" isMarginRight/>
        <Progress percent={50} type="circle" isMarginRight/>
        <Progress percent={70} type="circle" isMarginRight/>
        <Progress percent={100} type="circle" />
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="dashboard" isMarginRight/>
        <Progress percent={50} type="dashboard" isMarginRight/>
        <Progress percent={70} type="dashboard" isMarginRight/>
        <Progress percent={100} type="dashboard" />
      </Fragment>
    </Fragment>
  ))
  .add('without info', () => (
    <Fragment>
      <Fragment>
        <Progress percent={20} isShowInfo={false} isMarginBottom/>
        <Progress percent={50} isShowInfo={false} isMarginBottom/>
        <Progress percent={70} isShowInfo={false} isMarginBottom/>
        <Progress percent={100} isShowInfo={false}/>
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="circle" isShowInfo={false} isMarginRight/>
        <Progress percent={50} type="circle" isShowInfo={false} isMarginRight/>
        <Progress percent={70} type="circle" isShowInfo={false} isMarginRight/>
        <Progress percent={100} type="circle" isShowInfo={false}/>
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="dashboard" isShowInfo={false} isMarginRight/>
        <Progress percent={50} type="dashboard" isShowInfo={false} isMarginRight/>
        <Progress percent={70} type="dashboard" isShowInfo={false} isMarginRight/>
        <Progress percent={100} type="dashboard" isShowInfo={false}/>
      </Fragment>
    </Fragment>
  ))
  .add('with format', () => (
    <Fragment>
      <Fragment>
        <Progress percent={20} isMarginBottom format={percent => `${percent} year`}/>
        <Progress percent={50} isMarginBottom format={percent => `${percent} year`}/>
        <Progress percent={70} isMarginBottom format={percent => `${percent} year`}/>
        <Progress percent={100} format={percent => `${percent} year`}/>
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="circle" isMarginRight format={percent => `${percent} year`}/>
        <Progress percent={50} type="circle" isMarginRight format={percent => `${percent} year`}/>
        <Progress percent={70} type="circle" isMarginRight format={percent => `${percent} year`}/>
        <Progress percent={100} type="circle" format={percent => `${percent} year`}/>
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="dashboard" isMarginRight format={percent => `${percent} year`}/>
        <Progress percent={50} type="dashboard" isMarginRight format={percent => `${percent} year`}/>
        <Progress percent={70} type="dashboard" isMarginRight format={percent => `${percent} year`}/>
        <Progress percent={100} type="dashboard" format={percent => `${percent} year`}/>
      </Fragment>
    </Fragment>
  ))
  .add('with status', () => (
    <Fragment>
      <Fragment>
        <Progress percent={20} isMarginBottom status="success"/>
        <Progress percent={50} isMarginBottom status="normal"/>
        <Progress percent={70} isMarginBottom status="exception"/>
        <Progress percent={100} status="active"/>
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="circle" isMarginRight status="success"/>
        <Progress percent={50} type="circle" isMarginRight status="exception"/>
        <Progress percent={70} type="circle" isMarginRight status="normal"/>
        <Progress percent={100} type="circle" status="exception"/>
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="dashboard" isMarginRight status="success"/>
        <Progress percent={50} type="dashboard" isMarginRight status="exception"/>
        <Progress percent={70} type="dashboard" isMarginRight status="normal"/>
        <Progress percent={100} type="dashboard" status="exception"/>
      </Fragment>
    </Fragment>
  ))
  .add('with stroke width', () => (
    <Fragment>
      <Fragment>
        <Progress percent={20} isMarginBottom strokeWidth={2}/>
        <Progress percent={50} isMarginBottom strokeWidth={5}/>
        <Progress percent={70} isMarginBottom/>
        <Progress percent={100} strokeWidth={15}/>
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="circle" isMarginRight strokeWidth={2}/>
        <Progress percent={50} type="circle" isMarginRight strokeWidth={5}/>
        <Progress percent={70} type="circle" isMarginRight/>
        <Progress percent={100} type="circle" strokeWidth={15}/>
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="dashboard" isMarginRight strokeWidth={2}/>
        <Progress percent={50} type="dashboard" isMarginRight strokeWidth={5}/>
        <Progress percent={70} type="dashboard" isMarginRight/>
        <Progress percent={100} type="dashboard" strokeWidth={15}/>
      </Fragment>
    </Fragment>
  ))
  .add('stroke cap types', () => (
    <Fragment>
      <Fragment>
        <Progress percent={20} isMarginBottom/>
        <Progress percent={50} isMarginBottom strokeLinecap="square"/>
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="circle" isMarginRight/>
        <Progress percent={50} type="circle" isMarginRight strokeLinecap="square"/>
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="dashboard" isMarginRight/>
        <Progress percent={50} type="dashboard" isMarginRight strokeLinecap="square"/>
      </Fragment>
    </Fragment>
  ))
  .add('custom stroke colors', () => (
    <Fragment>
      <Fragment>
        <Progress percent={20} isMarginBottom/>
        <Progress percent={50} isMarginBottom strokeColor={{ '0%': '#108ee9', '100%': '#87d068' }}/>
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="circle" isMarginRight/>
        <Progress percent={50} type="circle" isMarginRight strokeColor={{ '0%': '#108ee9', '100%': '#87d068' }}/>
      </Fragment>
      <Divider/>
      <Fragment>
        <Progress percent={20} type="dashboard" isMarginRight/>
        <Progress percent={50} type="dashboard" isMarginRight strokeColor={{ '0%': '#108ee9', '100%': '#87d068' }}/>
      </Fragment>
    </Fragment>
  ))
  .add('with custom width', () => (
    <Fragment>
        <Progress percent={20} type="circle" isMarginRight width={200}/>
        <Progress percent={50} type="circle" width={300}/>
        <Divider/>
        <Progress percent={20} type="dashboard" isMarginRight width={200}/>
        <Progress percent={50} type="dashboard" width={300}/>
    </Fragment>
  ))
