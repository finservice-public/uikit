import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import AntdButton from 'antd/es/button'
import Icon from 'ui/Icon'
import styled from 'styled-components'
import withMargin from 'ui/common/withMargin'
import { Context } from 'ui/Provider/Context'
import { safeObject } from 'utils/object'
import { applyHtmlData } from 'utils/htmlData'

const Root = styled.span`
  display: inline-flex !important;
  justify-content: space-between;
  align-items: center;
`

const Prefix = styled.div`
  margin-left: ${({ isLeft, isMargin }) => (!isLeft && isMargin ? '14px' : 'inherit')};
  margin-right: ${({ isLeft, isMargin }) => (isLeft && isMargin ? '14px' : 'inherit')};
`

const Button = ({
  className, style, htmlID, dataSet,

  left, right, icon, iconType, iconPlacement, isLoading,
  href, uri, target, size, type, isDisable, isRound,
  children, onClick, isFill, tabIndex
}) => {
  const { Link } = useContext(Context)

  const iconComponent = icon && !isLoading ?
      <Icon
        icon={icon}
        iconType={iconType}
      /> : null

  const component =
      <AntdButton
          ghost={!isFill}
          onClick={onClick}
          style={safeObject({
            ...style,
            borderWidth: isLoading && type !== 'default' ? 0 : null
          })}
          className={className}
          disabled={isDisable}
          {...safeObject({
            type: type !== 'default' ? type : 'ghost',
            shape: isRound ? 'round' : null,
            size: size === 'lg' ? 'large' : size === 'xs' ? 'small' : null,
            id: htmlID || null,
            tabIndex
          })}
          {...applyHtmlData(dataSet)}
          loading={isLoading}
      >
        <Root>
          {((iconComponent && iconPlacement === 'left') || left) &&
          <Prefix isMargin={!!children} isLeft>
            {iconComponent || left}
          </Prefix>
          }
          {children}
          {((iconComponent && iconPlacement === 'right') || right) &&
          <Prefix isMargin={!!children} isLeft={false}>
            {iconComponent || right}
          </Prefix>
          }
        </Root>
      </AntdButton>

  return (
    !href ? component : !target ? <Link to={href} href={href} uri={uri}>{component}</Link> :
          <a href={href} target={target}>{component}</a>
  )
}

Button.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  type: PropTypes.oneOf(['default', 'primary', 'danger']),
  isFill: PropTypes.bool,
  left: PropTypes.any,
  right: PropTypes.any,
  icon: PropTypes.string,
  iconType: PropTypes.oneOf(['design', 'redesign', 'ant']),
  iconPlacement: PropTypes.oneOf(['left', 'right']),
  isLoading: PropTypes.bool,
  isDisable: PropTypes.bool,
  href: PropTypes.string,
  uri: PropTypes.string,
  target: PropTypes.string,
  size: PropTypes.oneOf(['xs', 'sm', 'lg']),
  isRound: PropTypes.bool,
  children: PropTypes.any,
  onClick: PropTypes.func,
  tabIndex: PropTypes.any
}

Button.defaultProps = {
  type: 'default',
  isFill: true,
  left: null,
  right: null,
  icon: null,
  iconType: 'ant',
  iconPlacement: 'left',
  isLoading: false,
  isDisable: false,
  href: '',
  target: '',
  size: 'lg',
  isRound: false,
  color: 'default',
  children: null,
  className: '',
  style: null,
  tabIndex: null
}

const ButtonGroup = withMargin({ displayName: 'ButtonGroup', forceIsRem: false })(AntdButton.Group)

Button.displayName = 'Button'
ButtonGroup.displayName = 'ButtonGroup'

export { ButtonGroup }

export default withMargin({ displayName: 'Button' })(Button)
