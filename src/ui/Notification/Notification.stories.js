import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import Button from 'ui/Button'
import { notifyError, notifySuccess, notifyInfo, notifyWarning } from 'utils/notification'

const stories = storiesOf('Notification', module)

const description = 'Example description'

stories
  .add('types', () => (
    <>
        <Button
            onClick={() => notifySuccess('Success', description)}
            isMarginRight
        >
            Success
        </Button>
        <Button
            onClick={() => notifyInfo('Info', description)}
            isMarginRight
        >
            Info
        </Button>
        <Button
            onClick={() => notifyError('Error', description)}
            isMarginRight
        >
            Error
        </Button>
        <Button
            onClick={() => notifyWarning('Warning', description)}
        >
            Warning
        </Button>
    </>
  ))
