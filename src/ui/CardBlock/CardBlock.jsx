import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import Card from 'antd/es/card'
import Tooltip from 'antd/es/tooltip'
import Badge from 'antd/es/badge'
import Loader from 'ui/Loader'
import styled from 'styled-components'
import { colors } from 'styles'
import withMargin from 'ui/common/withMargin'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Root = styled.div`
  position: relative;
  cursor: default !important;
  color: ${colors.default};
`

const Title = styled.div`
  color: ${props => colors[props.color]}
`

const TitleCount = styled(Badge)`
  margin-top: -14px !important;
  margin-left: 3px !important;
`

const LoaderContainer = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: not-allowed;
`

const SpinLoader = styled(Loader)`
  display: block;
`

const CardContainer = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isLoading')
  _.unset(resolveProps, 'isRem')
  _.unset(resolveProps, 'withoutSidePadding')
  return <Card
      {...resolveProps}
      headStyle ={props.withoutSidePadding ? { padding: 0 } : {}}
      bodyStyle ={props.withoutSidePadding ? { padding: '24px 0' } : {}}
  />
})`
  opacity: ${props => (props.isLoading ? 0.5 : 1)};
  cursor: default !important;
`

const CardBlock = ({
  className, style, htmlID, dataSet,

  count, title, onClick, isLoading, color, isHover, withoutSidePadding = false,
  ...props
}) => {
  const cardProps = {
    className,
    onClick,
    ...props
  }
  _.unset(cardProps, 'isRem')
  _.unset(cardProps, 'marginTop')
  _.unset(cardProps, 'marginBottom')
  _.unset(cardProps, 'marginLeft')
  _.unset(cardProps, 'marginRight')

  if (count) {
    cardProps.title =
        <Title color={color} isStrong>
          <Tooltip title={`Всего: ${count}`}>
            <span>{title}</span>
            <TitleCount
              count={count}
              overflowCount={10000000}
              style={{
                backgroundColor: colors.success
              }}
            />
          </Tooltip>
        </Title>
  } else if (title) {
    cardProps.title = (
      <Title color={color} isStrong>
        {title}
      </Title>
    )
  }

  return (
      <Root
        className={className}
        style={style}
        {...safeObject({
          id: htmlID || null
        })}
        {...applyHtmlData(dataSet)}
      >
        <CardContainer
          hoverable={isHover}
          isLoading={isLoading}
          withoutSidePadding={withoutSidePadding}
          {...cardProps}
        />
        {isLoading &&
        <LoaderContainer>
          <SpinLoader />
        </LoaderContainer>
        }
      </Root>
  )
}

CardBlock.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  title: PropTypes.any,
  count: PropTypes.number,
  isLoading: PropTypes.bool,
  onClick: PropTypes.func,
  isHover: PropTypes.bool,
  color: PropTypes.oneOf(['primary', 'info', 'success', 'danger', 'warning', 'default']),
  withoutSidePadding: PropTypes.bool
}

CardBlock.defaultProps = {
  className: '',
  style: {},
  count: 0,
  isLoading: false,
  color: 'primary',
  isHover: true,
  withoutSidePadding: false
}

CardBlock.displayName = 'CommonCardBlock'

export default withMargin({ displayName: 'CardBlock' })(CardBlock)
