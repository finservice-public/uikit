import fp from 'lodash/fp'
import React from 'react'
import { storiesOf } from '@storybook/react'
import CardBlock from './CardBlock'

const stories = storiesOf('CardBlock', module)

stories.addDecorator(story => (
    <div style={{ width: '70%' }}>
        {story()}
    </div>
))

const ChildrenComponent = () => (
    <div>Any children</div>
)

fp.forEach(color => {
  stories.add(`title with color: ${color}`, () => (
    <CardBlock color={color} title="Title">
      <ChildrenComponent/>
    </CardBlock>
  ))
})(['default', 'primary', 'info', 'success', 'error', 'warning'])


stories
  .add('without title', () => (
    <CardBlock>
        <ChildrenComponent/>
    </CardBlock>
  ))
  .add('loading', () => (
    <CardBlock isLoading title="Title">
      <ChildrenComponent/>
    </CardBlock>
  ))
  .add('no hover', () => (
    <CardBlock title="Title" isHover={false}>
      <ChildrenComponent/>
    </CardBlock>
  ))
  .add('with count', () => (
    <CardBlock title="Title" count={200}>
        <ChildrenComponent/>
    </CardBlock>
  ))
