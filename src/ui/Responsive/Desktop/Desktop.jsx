import React, { useContext, useState } from 'react'
import { Context } from 'ui/Provider/Context'
import styled from 'styled-components'
import Block from 'ui/Block'
import { mountHook } from 'hooks/mount'
import { viewPortsResolve } from 'styles'

const Root = styled(Block)`
  display: none;
  
  ${props => viewPortsResolve(props.breakPoints).desktop} {
    display: block;
  }
  
  ${props => viewPortsResolve(props.breakPoints).beforeDesktop} {
    display: block;
  }
  
  ${props => viewPortsResolve(props.breakPoints).touch} {
    display: none;
  }
`

const Desktop = ({ children, ...props }) => {
  const [isInit, setIsInit] = useState(false)
  const { responsiveModel: { isDesktop, breakPoints } } = useContext(Context)

  mountHook(() => {
    setIsInit(true)
  })

  const component = <Root breakPoints={breakPoints} {...props}>{children}</Root>
  return !isInit ? component :
    isDesktop ? component : null
}

export default Desktop
