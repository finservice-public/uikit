import React, { Fragment } from 'react'
import { storiesOf } from '@storybook/react'
import Loader from './Loader'

const stories = storiesOf('Loader', module)

stories
  .add('default', () => (
    <Loader />
  ))
  .add('without color', () => (
    <Loader isColor={false}/>
  ))
  .add('sizes', () => (
    <Fragment>
        <Loader/>
        <br/>
        <Loader size={3}/>
        <br/>
        <Loader size={4}/>
    </Fragment>
  ))
