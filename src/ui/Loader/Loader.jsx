import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import Spin from 'antd/es/spin'
import { colors } from 'styles'
import styled, { keyframes } from 'styled-components'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const animationFuncRootRotate = keyframes`
  0% {
    transform: translate(0%);
  }
  25% {
    transform: scale(0.5) rotate(90deg); 
  }
  50% {
    transform: translate(0%);
  }
  75% {
    transform: scale(0.5) rotate(275deg);
  }
`

const animationFuncRoot = keyframes`
  0% {
    transform: translate(0%);
  }
  25% {
    transform: scale(0.5); 
  }
  50% {
    transform: translate(0%);
  }
  75% {
    transform: scale(0.5);
  }
`

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'isRotate')
  return <div {...resolveProps}/>
})`
  animation: ${props => (props.isRotate ? animationFuncRootRotate : animationFuncRoot)} 2s infinite ease-in-out;
  display: inline-block;

  & svg {
    max-width: 100%;
    max-height: 100%;
  }
`

const animationFunc = keyframes`
  0% {
    opacity: 0.4;
  }
  100% {
    opacity: 1;
  }
`

const One = styled.path`
  transition: opacity .3s ease-in-out;
  animation: ${animationFunc} 2s infinite ease-in-out;
  animation-delay: 1.5s;
`

const Two = styled.path`
  transition: opacity .3s ease-in-out;
  animation: ${animationFunc} 2s infinite ease-in-out;
  animation-delay: 1s;
`

const Three = styled.path`
  transition: opacity .3s ease-in-out;
  animation: ${animationFunc} 2s infinite ease-in-out;
  animation-delay: 0.5s;
`

const Four = styled.path`
  transition: opacity .3s ease-in-out;
  animation: ${animationFunc} 2s infinite ease-in-out;
`

const Loader = ({
  className, style, htmlID, dataSet,

  size, isColor, isRotate
}) => (
    <Root
        className={className}
        style={{
          ...style,
          width: `${size * 14}px`,
          height: `${size * 14}px`
        }}
        {...safeObject({
          id: htmlID || null
        })}
        {...applyHtmlData(dataSet)}
        isRotate={isRotate}
    >
      <svg viewBox="0 0 42 41">
        <defs>
          <linearGradient id="loader-gradient-1" x1="62.475%" x2="37.48%" y1="62.515%" y2="37.489%">
            <stop offset="0%" stopColor="#3F7BBF"/>
            <stop offset="100%" stopColor="#00B7F4"/>
          </linearGradient>
          <linearGradient id="loader-gradient-2" x1="-.002%" x2="99.996%" y1="50.001%" y2="50.001%">
            <stop offset="0%" stopColor="#445FA9"/>
            <stop offset="100%" stopColor="#3F7BBF"/>
          </linearGradient>
        </defs>
        <g fill="none" fillRule="nonzero">
          <One fill={isColor ? '#00B7F4' : colors.gray3} d="M21.055 14.656c-3.8 0-6.88-3.076-6.88-6.871 0-3.795 3.08-6.871 6.88-6.871s6.88 3.076 6.88 6.87c0 3.796-3.08 6.872-6.88 6.872z"/>
          <Two fill={isColor ? '#3F7BBF' : colors.gray3} d="M34.315 27.926c-3.8 0-6.88-3.076-6.88-6.871 0-3.795 3.08-6.871 6.88-6.871s6.88 3.076 6.88 6.87c0 3.796-3.08 6.872-6.88 6.872z"/>
          <Three fill={isColor ? '#445FA9' : colors.gray3} d="M21.055 41.003c-3.8 0-6.88-3.076-6.88-6.87 0-3.795 3.08-6.872 6.88-6.872s6.88 3.077 6.88 6.871c0 3.795-3.08 6.871-6.88 6.871z"/>
          <Four fill={isColor ? '#000' : colors.gray3} d="M7.795 27.926c-3.8 0-6.88-3.076-6.88-6.871 0-3.795 3.08-6.871 6.88-6.871s6.88 3.076 6.88 6.87c0 3.796-3.08 6.872-6.88 6.872z"/>
        </g>
      </svg>
    </Root>
)

Loader.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  isColor: PropTypes.bool,
  isRotate: PropTypes.bool,

  size: PropTypes.number
}

Loader.defaultProps = {
  size: 2,
  isColor: true,
  isRotate: false
}

const LoaderComponent = Loader

LoaderComponent.displayName = 'Loader'

Spin.setDefaultIndicator(<LoaderComponent/>)

export default LoaderComponent
