import React from 'react'
import PropTypes from 'prop-types'
import AntTag from 'antd/es/tag'
import { colors } from 'styles'
import withMargin from 'ui/common/withMargin'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Tag = ({
  className, style, htmlID, dataSet,

  color, textColor, isMedium,

  onClose,
  children
}) => (
     <AntTag
        className={className}
        style={safeObject({
          ...style,
          color: textColor ? colors[textColor] ? colors[textColor] : textColor : null,
          fontWeight: isMedium ? 500 : null
        })}
        onClose={onClose}
        {...safeObject({
          color: color ? colors[color] ? colors[color] : color : null,
          closable: onClose ? true : null,
          id: htmlID || null
        })}
        {...applyHtmlData(dataSet)}
     >
         {children}
     </AntTag>
)

Tag.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  color: PropTypes.string, // color of theme or custom color
  textColor: PropTypes.string, // color of theme or custom color

  isMedium: PropTypes.bool,

  onClose: PropTypes.func
}

Tag.defaultProps = {
  color: null,
  isMedium: false
}

Tag.displayName = 'Tag'

export default withMargin({ displayName: 'Tag' })(Tag)
