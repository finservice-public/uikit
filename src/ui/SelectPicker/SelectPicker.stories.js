import React, { Fragment, useState } from 'react'
import { storiesOf } from '@storybook/react'
import storiesHelpers from 'ui/common/withFormProps/storiesHelpers'
import { action } from '@storybook/addon-actions'
import SelectPicker from 'ui/SelectPicker'

const stories = storiesOf('SelectPicker', module)

stories.addDecorator(story => (
  <div style={{ width: '40rem' }}>
    {story()}
  </div>
))

const options = [
  { value: 'one', label: 'One' },
  { value: 'two', label: 'Two' },
  { value: 'three', label: 'Three' },
  { value: 'four', label: 'Four' }
]

stories
  .add('default', () => {
    const [value, setValue] = useState('one')
    return (
        <SelectPicker options={options} onChange={setValue} value={value}/>
    )
  })
  .add('with placeholder', () => (
    <SelectPicker placeholder="Select any" options={options} onChange={action('changed')}/>
  ))
  .add('sizes', () => (
    <Fragment>
      <SelectPicker options={options} onChange={action('changed')} size="xs" isMarginBottom/>
      <SelectPicker options={options} onChange={action('changed')} size="sm" isMarginBottom/>
      <SelectPicker options={options} onChange={action('changed')} size="lg"/>
    </Fragment>
  ))
  .add('disabled', () => (
    <SelectPicker options={options} onChange={action('changed')} isDisable/>
  ))
  .add('with htmlID', () => (
    <SelectPicker options={options} onChange={action('changed')} htmlID="any-picker"/>
  ))

storiesHelpers({
  stories,
  Component: props => <SelectPicker options={options} onChange={action('changed')} {...props}/>
})
