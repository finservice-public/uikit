import React from 'react'
import { storiesOf } from '@storybook/react'
import NoData from './NoData'

const stories = storiesOf('NoData', module)

stories.addDecorator(story => (
    <div style={{ width: '40rem' }}>
        {story()}
    </div>
))

stories
  .add('default', () => (
        <NoData/>
  ))
