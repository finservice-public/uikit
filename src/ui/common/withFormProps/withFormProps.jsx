import _ from 'lodash'
import React, { useContext, useState } from 'react'
import PropTypes from 'prop-types'
import Form from 'antd/es/form'
import styled from 'styled-components'
import { colors } from 'styles'
import { Context } from 'ui/Provider/Context'
import { safeObject } from 'utils/object'
import { designInputMarginBottom } from './constants'

const Root = styled(props => {
  const resolveProps = { ...props }
  _.unset(resolveProps, 'weight')
  _.unset(resolveProps, 'errorWidth')
  _.unset(resolveProps, 'isMarginBottom')
  return <Form.Item {...resolveProps}/>
})`
  padding-bottom: ${props => (props.isMarginBottom ? `${designInputMarginBottom}px` : 0)} !important;
  margin-bottom: 0 !important;

  & .ant-form-item-label {
    font-size: 14px !important;
    padding-bottom: 0 !important;
  }

  & label {
    margin-bottom: 0;
    font-weight: ${props => props.weight};
  }
  
  & .ant-form-explain {
    position: absolute;
    width: ${props => (props.errorWidth ? `${props.errorWidth}px` : 'auto')}
  }
  
  & .ant-form-item-label {
    text-align: left;
    line-height: 1.7 !important;
  } 
`

const withFormProps = options => OriginalComponent => {
  const displayName = _.get(options, 'displayName', 'ResolveComponent')
  const isExistError = _.get(options, 'isExistError', true)
  const isExistWarning = _.get(options, 'isExistWarning', true)
  const isExistHelp = _.get(options, 'isExistHelp', true)

  const ResolveComponent = ({
    className, style,

    onFocus, onBlur, errorWidth,

    isFullWidth, isForm, label,
    isShowForceValidate, isMarginForHelp,

    ...props
  }) => {
    const weight = _.get(useContext(Context), 'label.weight', 600)

    const [isInput, setIsInput] = useState(false)
    const handleChangeIsInput = current => {
      setIsInput(current)
    }

    const [isFocus, setIsFocus] = useState(false)
    const handleChangeIsFocus = current => {
      setIsFocus(current)
      if (current && onFocus) {
        onFocus()
      } else if (!current && onBlur) {
        onBlur()
      }
    }

    const isOnlyShowForceValidate = _.get(options, 'isOnlyShowForceValidate', false)
    const isShowValidate = !isOnlyShowForceValidate ? isShowForceValidate : true
    const isShowErrorOrWarn = (isExistError || isExistWarning) &&
        ((!!isShowValidate && (props.error || props.warn)) || (isInput && (props.error || props.warn)))
    const error = isShowErrorOrWarn ? props.error : null
    const warn = isShowErrorOrWarn ? props.warn : null
    const help = !isFocus && isExistHelp ? (error || (warn || (props.help ?
        <div style={{ color: colors.gray4 }}>{props.help}</div> :
        <div style={{ display: 'none' }}/>))) :
        <div style={{ display: 'none' }}/>

    const isExistForm = isForm || !!label || !!error || !!warn || !!props.help
    const width = _.get(style, 'width', null)

    return (
      isExistForm ?
        <Root
            label={label || null}
            className={className}
            errorWidth={errorWidth}
            weight={weight}
            style={safeObject({
              width: isFullWidth && !width ? '100%' : width,
              ...style
            })}
            colon={false}
            labelCol={{
              xs: 12,
              md: 12
            }}
            {...safeObject({
              help,
              validateStatus: isShowErrorOrWarn && !isFocus ? error ? 'error' : warn ? 'warning' : null : null
            })}
            isMarginBottom={isMarginForHelp}
        >
          <OriginalComponent
              onChangeIsInput={handleChangeIsInput}
              onChangeIsFocus={handleChangeIsFocus}
              isShowForceValidate={isShowForceValidate}
              onFocus={onFocus}
              onBlur={onBlur}
              {...props}
          />
        </Root> :
        <OriginalComponent
            className={className}
            style={style}
            onFocus={onFocus}
            onBlur={onBlur}
            {...props}
        />
    )
  }

  ResolveComponent.displayName = `${displayName}Form`

  ResolveComponent.propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,

    label: PropTypes.any,
    isFullWidth: PropTypes.bool,
    isForm: PropTypes.bool,

    error: PropTypes.any,
    errorWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    warn: PropTypes.any,
    help: PropTypes.any,
    isShowForceValidate: PropTypes.bool,
    isMarginForHelp: PropTypes.bool
  }

  ResolveComponent.defaultProps = {
    label: null,
    isFullWidth: true,
    errorWidth: null,
    isForm: false,

    error: null,
    warn: null,
    help: null,
    isShowForceValidate: false,
    isMarginForHelp: true
  }

  return ResolveComponent
}

export default withFormProps
