import React from 'react'
import { storiesOf } from '@storybook/react'
import Input from 'ui/Input'

const stories = storiesOf('Margin', module)

stories.addDecorator(story => (
    <div style={{ width: '80%' }}>
        {story()}
    </div>
))

stories
  .add('default margin left', () => (
    <div style={{ display: 'flex' }}>
        <Input label="Label"/>
        <Input isMarginLeft label="Label"/>
        <Input isMarginLeft label="Label"/>
        <Input isMarginLeft label="Label"/>
    </div>
  ))
  .add('custom margin left', () => (
    <div style={{ display: 'flex' }}>
        <Input label="Label"/>
        <Input isMarginLeft marginLeft={3} label="Label"/>
        <Input isMarginLeft marginLeft={3} label="Label"/>
        <Input isMarginLeft marginLeft={3} label="Label"/>
    </div>
  ))
  .add('default margin right', () => (
    <div style={{ display: 'flex' }}>
        <Input isMarginRight label="Label"/>
        <Input isMarginRight label="Label"/>
        <Input isMarginRight label="Label"/>
        <Input label="Label"/>
    </div>
  ))
  .add('custom margin right', () => (
    <div style={{ display: 'flex' }}>
        <Input isMarginRight marginRight={3} label="Label"/>
        <Input isMarginRight marginRight={3} label="Label"/>
        <Input isMarginRight marginRight={3} label="Label"/>
        <Input label="Label"/>
    </div>
  ))
  .add('default margin bottom', () => (
    <div>
        <Input isMarginBottom label="Label"/>
        <Input isMarginBottom label="Label"/>
        <Input isMarginBottom label="Label"/>
        <Input label="Label"/>
    </div>
  ))
  .add('custom margin bottom', () => (
    <div>
        <Input isMarginBottom marginBottom={3} label="Label"/>
        <Input isMarginBottom marginBottom={3} label="Label"/>
        <Input isMarginBottom marginBottom={3} label="Label"/>
        <Input label="Label"/>
    </div>
  ))
  .add('default margin top', () => (
    <div>
        <Input label="Label"/>
        <Input isMarginTop label="Label"/>
        <Input isMarginTop label="Label"/>
        <Input isMarginTop label="Label"/>
    </div>
  ))
  .add('custom margin top', () => (
    <div>
        <Input label="Label"/>
        <Input isMarginTop marginTop={3} label="Label"/>
        <Input isMarginTop marginTop={3} label="Label"/>
        <Input label="Label" isMarginTop marginTop={3}/>
    </div>
  ))
