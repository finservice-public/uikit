import _ from 'lodash'
import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import { Context } from 'ui/Provider/Context'
import { safeObject, isSafe } from 'utils/object'
import { r2px } from 'utils/style'

const withMargin = options => OriginalComponent => {
  const displayName = _.get(options, 'displayName', 'ResolveComponent')
  const forceIsRem = _.get(options, 'forceIsRem', true)

  const ResolveComponent = ({
    style, width, height,
    isMarginTop, isMarginBottom, isMarginLeft, isMarginRight, isRem,
    ...props
  }) => {
    const isWidth = _.get(options, 'isWidth', true)
    const isHeight = _.get(options, 'isHeight', true)
    const { remPoint } = useContext(Context)
    const point = isRem ? remPoint : 1

    const marginTop = isMarginTop ? r2px(props.marginTop || 1, point) : isSafe(props.marginTop) ? r2px(props.marginTop, point) : null
    const marginBottom = isMarginBottom ? r2px(props.marginBottom || 1, point) : isSafe(props.marginBottom) ? r2px(props.marginBottom, point) : null
    const marginLeft = isMarginLeft ? r2px(props.marginLeft || 1, point) : isSafe(props.marginLeft) ? r2px(props.marginLeft, point) : null
    const marginRight = isMarginRight ? r2px(props.marginRight || 1, point) : isSafe(props.marginRight) ? r2px(props.marginRight, point) : null

    return (
        <OriginalComponent
            style={{
              ...style,
              ...safeObject({
                marginTop,
                marginBottom,
                marginLeft,
                marginRight,
                width: width && isWidth ? _.isNumber(width) ? `${width * point}px` : width : null,
                height: height && isHeight ? _.isNumber(height) ? `${height * point}px` : height : null
              })
            }}
            {...safeObject({
              width: !isWidth ? width : null,
              height: !isHeight ? height : null,
              isRem: forceIsRem ? isRem : null
            })}
            {...props}
        />
    )
  }

  ResolveComponent.displayName = `${displayName}Margin`

  ResolveComponent.propTypes = {
    style: PropTypes.object,
    isRem: PropTypes.bool,
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    isMarginTop: PropTypes.bool,
    marginTop: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    isMarginBottom: PropTypes.bool,
    marginBottom: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    isMarginLeft: PropTypes.bool,
    marginLeft: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    isMarginRight: PropTypes.bool,
    marginRight: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  }

  ResolveComponent.defaultProps = {
    width: null,
    height: null,
    isMarginTop: false,
    isMarginBottom: false,
    isMarginLeft: false,
    isMarginRight: false,
    isRem: true
  }

  return ResolveComponent
}

export default withMargin
