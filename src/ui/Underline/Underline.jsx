import React from 'react'
import withMargin from 'ui/common/withMargin'
import Block from 'ui/Block'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { colors } from 'styles'

const Root = styled(Block)`
  transition: color .3s ease-in-out;
  
  &:hover {
    color: ${colors.primary};
  }
`

const Under = styled(Block)`
  width: auto;
  height: 2px;
  border-radius: 1px;
  background: ${props => colors[props.color] || props.color};
`

const UnderLine = ({ className, style, color, onClick, isInline, htmlID, children }) => (
  <Root
    onClick={onClick}
    isInline={isInline}
    className={className}
    style={style}
    htmlID={htmlID}
  >
    {children}
    <Under color={color}/>
  </Root>
)

UnderLine.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  color: PropTypes.string, // color of theme or custom
  onClick: PropTypes.func,
  isInline: PropTypes.bool,
  htmlID: PropTypes.any
}

UnderLine.defaultProps = {
  color: 'primary',
  isInline: true
}

UnderLine.displayName = 'UnderLine'

export default withMargin({ displayName: 'UnderLine' })(UnderLine)
