import _ from 'lodash'
import React from 'react'
import PropTypes from 'prop-types'
import AntDrawer from 'antd/es/drawer'
import styled from 'styled-components'
import { Divider } from 'ui/Typography'
import { colors } from 'styles'
import { applyHtmlData } from 'utils/htmlData'
import { safeObject } from 'utils/object'

const Footer = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 0;
  background: white;
`

const FooterContent = styled.div`
  padding: 0 24px 24px 24px;
`

const Drawer = ({
  className, style, htmlID, dataSet,

  isOpen, placement, title, titleColor, isClosable, width, height, bodyStyle, footer,
  onClose, children
}) => (
    <AntDrawer
        className={className}
        style={style}
        visible={isOpen}
        onClose={onClose}
        placement={placement}
        closable={isClosable && isOpen}
        width={width}
        height={height}
        bodyStyle={bodyStyle}
        {...safeObject({
          id: htmlID || null,
          title: title ?
              <span style={{ color: colors[titleColor] }}>
                {title}
              </span> : null
        })}
        {...applyHtmlData(dataSet)}
    >
      {children}
      {footer && isOpen &&
      <Footer>
        <Divider/>
        <FooterContent>
          {footer}
        </FooterContent>
      </Footer>
      }
    </AntDrawer>
)

Drawer.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  htmlID: PropTypes.any,
  dataSet: PropTypes.object,

  isOpen: PropTypes.bool,
  placement: PropTypes.oneOf(['top', 'left', 'bottom', 'right']),
  titleColor: PropTypes.oneOf(['primary', 'info', 'success', 'danger', 'warning', 'default']),
  title: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  isClosable: PropTypes.bool,
  bodyStyle: PropTypes.object,
  footer: PropTypes.any,

  onClose: PropTypes.func
}

Drawer.defaultProps = {
  isOpen: false,
  placement: 'left',
  titleColor: 'primary',
  title: '',
  width: 256,
  isClosable: true,
  footer: null
}

Drawer.displayName = 'Drawer'

export default Drawer
