import React from 'react'

const Icon = props => <svg width="1em" height="1em" viewBox="0 0 16 16" {...props}><path fill="currentColor" fillRule="evenodd" d="M8 12c-.237 0-.433-.082-.622-.247L2.237 7.186C2.087 7.056 2 6.892 2 6.699 2 6.31 2.352 6 2.805 6c.223 0 .42.082.575.211l4.627 4.116L12.62 6.21c.162-.135.365-.211.582-.211.446 0 .798.311.798.699 0 .193-.074.358-.23.487l-5.14 4.567c-.177.165-.386.241-.63.247z" /></svg>

export default Icon
