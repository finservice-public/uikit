import U1b9448bc7dca409a8ea4eab5dc53a364 from './calc'
import Uaf23d7ee08eb48a69b59ba2ce0455b18 from './calendar'
import U5be9124c150244e1a9aae242fab071df from './camera'
import U49c3285887cc429b82dcd54833e9a1ba from './caret-down'
import U1cea7fb00dac4c408ac4cc9ccf88b8d8 from './chevron-down'
import U4953db4e8d0f444e8cb10b5c2eeec1fe from './cog'
import Uda58e4e886594c5cb3c337fcb029fed5 from './cogs'
import U4eedf627623a434582418030b897182d from './down'
import U40f39a86781f4375bdf4cbee3a376118 from './duplicate'
import U0aec86faa2b549f6a562753caa69e5eb from './edit'
import Ua628ab36c4cc4dff80f11d9de2410896 from './expand'
import U5c1fb41ddd3f46f99c704b4044ddb6c1 from './eye'
import U8725cf59c9b94f5fb11458a6d4c23212 from './globe'
import Ub04d5ec6896f4b38aa33eb34eaa981d5 from './home'
import Uf624919d1ddf46ccb05f07bcb2ec7663 from './level-down'
import U20111d560eb24693a6ab1e4f5a528c50 from './logo-marker'
import Ua2aa42b43bef44529503db5d900167c0 from './map'
import Uda409498f6d448e281942f1408d756ae from './menu'
import U0e92cc4163fd43bf94c0299a85c243c8 from './message'
import Ub1d46251ba35418c8e349db82b6a9c2c from './offer-details'
import Uc521886521e949b099ed2fcc3123eda6 from './phone'
import U2d52db45a4b44f2fb075efd45884c7ba from './question'
import U7923754a306641d7b2640ee55b840942 from './search'
import U3238f55d7471435a98516b6b446115e0 from './sort-up'
import U60f32444ad8748de98808bcdc61e69a5 from './sort'
import U24488cd848bc4792a36aa056eb93aaad from './trash'
import U59651d37eb1f433f96512725de2b82ad from './user'
import U50a8f76babda4016b303b2a59b79315c from './zoom-in'

export default {
  calc: U1b9448bc7dca409a8ea4eab5dc53a364,
  calendar: Uaf23d7ee08eb48a69b59ba2ce0455b18,
  camera: U5be9124c150244e1a9aae242fab071df,
  'caret-down': U49c3285887cc429b82dcd54833e9a1ba,
  'chevron-down': U1cea7fb00dac4c408ac4cc9ccf88b8d8,
  cog: U4953db4e8d0f444e8cb10b5c2eeec1fe,
  cogs: Uda58e4e886594c5cb3c337fcb029fed5,
  down: U4eedf627623a434582418030b897182d,
  duplicate: U40f39a86781f4375bdf4cbee3a376118,
  edit: U0aec86faa2b549f6a562753caa69e5eb,
  expand: Ua628ab36c4cc4dff80f11d9de2410896,
  eye: U5c1fb41ddd3f46f99c704b4044ddb6c1,
  globe: U8725cf59c9b94f5fb11458a6d4c23212,
  home: Ub04d5ec6896f4b38aa33eb34eaa981d5,
  'level-down': Uf624919d1ddf46ccb05f07bcb2ec7663,
  'logo-marker': U20111d560eb24693a6ab1e4f5a528c50,
  map: Ua2aa42b43bef44529503db5d900167c0,
  menu: Uda409498f6d448e281942f1408d756ae,
  message: U0e92cc4163fd43bf94c0299a85c243c8,
  'offer-details': Ub1d46251ba35418c8e349db82b6a9c2c,
  phone: Uc521886521e949b099ed2fcc3123eda6,
  question: U2d52db45a4b44f2fb075efd45884c7ba,
  search: U7923754a306641d7b2640ee55b840942,
  'sort-up': U3238f55d7471435a98516b6b446115e0,
  sort: U60f32444ad8748de98808bcdc61e69a5,
  trash: U24488cd848bc4792a36aa056eb93aaad,
  user: U59651d37eb1f433f96512725de2b82ad,
  'zoom-in': U50a8f76babda4016b303b2a59b79315c
}
