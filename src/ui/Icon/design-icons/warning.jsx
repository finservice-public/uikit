import React from 'react'

const Icon = props => <svg width="1em" height="1em" viewBox="0 0 16 15" {...props}><g fill="none" fillRule="nonzero"><path fill="currentColor" d="M1.97 15C.776 15 0 14.1 0 12.996c0-.329.085-.666.262-.97L6.308 1.01A1.896 1.896 0 0 1 8 0c.67 0 1.323.337 1.692 1.01l6.046 11.007c.177.313.262.65.262.979C16 14.1 15.223 15 14.038 15H1.97z" /><path fill="white" d="M8 9.593c-.484 0-.754-.256-.77-.72l-.123-3.559c-.017-.473.36-.814.885-.814.524 0 .918.349.893.822l-.123 3.542c-.024.473-.295.729-.762.729zM8 12.5c-.541 0-1-.364-1-.884s.45-.883 1-.883 1 .356 1 .883-.45.884-1 .884z" /></g></svg>

export default Icon
