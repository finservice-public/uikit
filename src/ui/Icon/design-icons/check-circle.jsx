import React from 'react'

const Icon = props => <svg width="1em" height="1em" viewBox="0 0 16 16" {...props}><g fill="none" fillRule="evenodd"><path fill="currentColor" d="M8 16c-4.38 0-8-3.62-8-7.996C0 3.619 3.613 0 7.992 0 12.372 0 16 3.62 16 8.004 16 12.38 12.38 16 8 16z" /><path fill="white" d="M6.451 9.635l4.624-4.617a.711.711 0 0 1 .946-.048.608.608 0 0 1 .085.868l-.036.04-5.11 5.1a.694.694 0 0 1-.331.186.71.71 0 0 1-.684-.182L3.93 8.97l-.036-.039a.608.608 0 0 1 .085-.868.711.711 0 0 1 .946.048L6.45 9.635z" /></g></svg>

export default Icon
