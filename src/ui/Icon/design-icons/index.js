import Ud2eae28114ee4c5e9301ca88504fd762 from './arrow-left'
import Ue5c0794f37124838a7019e2879a2f6fd from './briefcase'
import U37ee71637009429e9c4ff500e628aba2 from './call'
import U733226064ab642fba371fa35ce0d1931 from './check-circle'
import U956c25f6711a4c8081dae577d8d9f08f from './check'
import Ucc8ac485199640938cf9d960d054b7db from './circle-done'
import Ub194139e901f48689e8903fc8b8e7d83 from './close'
import U27e65e21cffb49f68b0ef13ce835ebb3 from './expand-up'
import Ub74eb9bf21f1448da623d2e775dc0318 from './hat'
import U7a13838ab9af40529bcca5a702388c21 from './heart'
import U53e98c4ec1fd47e19697ee6108209a85 from './info-circle'
import U461bec86f1e6416488c1fcf3eb04fd77 from './info'
import U6da0921aa3ca4b87babd172cc3815bc4 from './plus-extend'
import Ub801322756b74f9aa0c6e2b116ea3368 from './question'
import U2ab83ce2f9c14244b49a8bcffab0bf9f from './shield'
import U6d1a10adbc984bc48555a49255f28b38 from './sort-up'
import U81bc6176c344495eaeb9bb8294b2b91f from './sort'
import U834d4c58235441c786aad111b3e1195b from './warning'

export default {
  'arrow-left': Ud2eae28114ee4c5e9301ca88504fd762,
  briefcase: Ue5c0794f37124838a7019e2879a2f6fd,
  call: U37ee71637009429e9c4ff500e628aba2,
  'check-circle': U733226064ab642fba371fa35ce0d1931,
  check: U956c25f6711a4c8081dae577d8d9f08f,
  'circle-done': Ucc8ac485199640938cf9d960d054b7db,
  close: Ub194139e901f48689e8903fc8b8e7d83,
  'expand-up': U27e65e21cffb49f68b0ef13ce835ebb3,
  hat: Ub74eb9bf21f1448da623d2e775dc0318,
  heart: U7a13838ab9af40529bcca5a702388c21,
  'info-circle': U53e98c4ec1fd47e19697ee6108209a85,
  info: U461bec86f1e6416488c1fcf3eb04fd77,
  'plus-extend': U6da0921aa3ca4b87babd172cc3815bc4,
  question: Ub801322756b74f9aa0c6e2b116ea3368,
  shield: U2ab83ce2f9c14244b49a8bcffab0bf9f,
  'sort-up': U6d1a10adbc984bc48555a49255f28b38,
  sort: U81bc6176c344495eaeb9bb8294b2b91f,
  warning: U834d4c58235441c786aad111b3e1195b
}
