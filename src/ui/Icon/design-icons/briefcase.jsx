import React from 'react'

const Icon = props => <svg width="1em" height="1em" viewBox="0 0 16 14" {...props}><g fill="none" fillRule="evenodd"><path fill="currentColor" d="M4.994 2.008v.662h6.012v-.662c0-.527-.278-.797-.805-.797H5.8c-.527 0-.805.27-.805.797zM16 4.878v1.794H0V4.878C0 3.425.848 2.67 2.237 2.67h1.432v-.562C3.669.677 4.467 0 5.785 0h4.43c1.375 0 2.116.677 2.116 2.108v.562h1.432C15.21 2.67 16 3.425 16 4.878zM2.237 14C.784 14 0 13.238 0 11.792V7.72h5.115v.42c0 .634.37.997 1.004.997h3.74c.634 0 1.005-.363 1.005-.997v-.42H16v4.073C16 13.238 15.21 14 13.763 14H2.237z" /><path fill="white" d="M10.86 7.7v.54a.9.9 0 0 1-.9.9H6.01a.9.9 0 0 1-.9-.9V7.7H0v-1h16v1h-5.14zM5.7 1.21h4.6a.7.7 0 0 1 .7.7v.76H5v-.76a.7.7 0 0 1 .7-.7z" /></g></svg>

export default Icon
