const _ = require('lodash')
const path = require('path')
const getModule = require('../config/webpack/getModule')
const getPlugins = require('../config/webpack/getPlugins')
const constants = require('../config/webpack/constants')

module.exports = {
    stories: ['../src/**/*.stories.js'],
    webpackFinal: async config => {
        config.module = {
            ...config.module,
            ...getModule()
        }

        config.plugins = {
            ...config.plugins,

        }

        config.resolve = {
            ...config.resolve,
            alias: {
                ...config.resolve.alias,
                'react-dom': '@hot-loader/react-dom',
                ui: path.resolve(constants.SRC_PATH, 'ui'),
                styles: path.resolve(constants.SRC_PATH, 'styles'),
                utils: path.resolve(constants.SRC_PATH, 'utils'),
                resources: path.resolve(constants.SRC_PATH, 'resources'),
                hooks: path.resolve(constants.SRC_PATH, 'hooks')
            },
            extensions: ['.js', '.jsx', '.json', '.css', '.less', '.jpg', '.jpeg', '.png']
        }

        const plugins = []
        _.each(config.plugins, plugin => {
            plugins.push(plugin)
        })
        plugins.push(...getPlugins())
        config.plugins = plugins

        return config
    },
    addons: ['@storybook/addon-actions/register']
}
