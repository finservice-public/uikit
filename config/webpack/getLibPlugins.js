const webpack = require('webpack')
const config = require('../config')

module.exports = () => {
  const NODE_ENV = config.NODE_ENV

  if (NODE_ENV !== 'production') {
    return [
      new webpack.HotModuleReplacementPlugin()
    ]
  }
  return []
}
