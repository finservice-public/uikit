const path = require('path')
const config = require('../config')
const constants = require('./constants')

module.exports = () => {
  const NODE_ENV = config.NODE_ENV

  if (NODE_ENV === 'production') {
    return {
      path: path.resolve(constants.ROOT_PATH, 'lib'),
      filename: '[name].js',
      sourceMapFilename: '[name].js.map',
      publicPath: '/',
      libraryTarget: 'commonjs',
      library: ''
    }
  }
  return {
    path: path.resolve(constants.ROOT_PATH, 'lib'),
    filename: '[name].js',
    sourceMapFilename: '[name].js.map',
    publicPath: '/'
  }
}
