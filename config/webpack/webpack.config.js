const _ = require('lodash')
const TerserPlugin = require('terser-webpack-plugin')
const path = require('path')
const getPlugins = require('./getPlugins')
const getLibPlugins = require('./getLibPlugins')
const getEntry = require('./getEntry')
const getOutput = require('./getOutput')
const getModule = require('./getModule')
const config = require('../config')
const constants = require('./constants')

const NODE_ENV = config.NODE_ENV

module.exports = {
  mode: NODE_ENV === 'production' ? 'production' : 'development',
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: false,
        terserOptions: {
          ecma: 6
        }
      })
    ]
  },
  devtool: NODE_ENV === 'production' ? false : 'cheap-module-eval-source-map',
  entry: getEntry(),
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.css', '.less'],
    alias: _.omitBy({
      'react-dom': NODE_ENV !== 'production' ? '@hot-loader/react-dom' : null,
      'styled-components': require.resolve('styled-components'),

      ui: path.resolve(constants.SRC_PATH, 'ui'),
      styles: path.resolve(constants.SRC_PATH, 'styles'),
      utils: path.resolve(constants.SRC_PATH, 'utils'),
      resources: path.resolve(constants.SRC_PATH, 'resources'),
      hooks: path.resolve(constants.SRC_PATH, 'hooks')
    }, _.isNull)
  },
  output: getOutput(),
  plugins: [
    ...getPlugins(),
    ...getLibPlugins()
  ],
  module: getModule(),
  externals: {
    react: 'react',
    'react-dom': 'react-dom',
    'styled-components': {
      commonjs: 'styled-components',
      commonjs2: 'styled-components',
      amd: 'styled-components'
    },
    'prop-types': 'prop-types'
  }
}
