const webpack = require('webpack')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const config = require('../config')

module.exports = () => {
  const NODE_ENV = config.NODE_ENV

  const plugins = []

  plugins.push(new webpack.DefinePlugin({
    PRODUCTION: NODE_ENV === 'production'
  }))

  if (NODE_ENV === 'production') {
    plugins.push(new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }))
    plugins.push(new webpack.optimize.AggressiveMergingPlugin())
    plugins.push(new webpack.optimize.OccurrenceOrderPlugin())
  }

  plugins.push(new MiniCssExtractPlugin({
    filename: 'app.css'
  }))

  return plugins
}
